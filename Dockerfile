FROM node:20.12.2-alpine AS base

WORKDIR /srv/bannis-bot-discord

COPY package.json package-lock.json ./

ENV NODE_ENV=production

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
      make \
      g++ \
      jpeg-dev \
      cairo-dev \
      giflib-dev \
      pango-dev \
      libtool \
      autoconf \
      automake \
    && npm install --global npm@">=10.5.2 <11" \
    && npm install

FROM base as development

ENV NODE_ENV=development

RUN npm install

EXPOSE 9229

CMD ["npm", "run", "watch-and-start"]

FROM development as build

COPY . .

RUN npm run build

FROM node:20.12.2-alpine as production

WORKDIR /srv/bannis-bot-discord

ENV NODE_ENV=production

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
      cairo-dev \
      pango-dev \
      jpeg-dev \
      giflib-dev \
    && npm install --global npm@">=10.5.2 <11" \
    && npm install --global pm2@">=5.3.1 <6" \
    && mkdir /etc/bannis-bot-discord-sqlite \
    && chown -R node:node /srv/bannis-bot-discord /etc/bannis-bot-discord-sqlite

COPY package.json umzug.cli.js ./
COPY --from=build /srv/bannis-bot-discord/assets ./assets
COPY --from=build /srv/bannis-bot-discord/dist/src ./src
COPY --from=base /srv/bannis-bot-discord/node_modules ./node_modules

VOLUME /etc/bannis-bot-discord-sqlite

USER node

ENTRYPOINT ["pm2-runtime", "./src/app.js"]
