<div align="center">
  <br />
    <h2>BOT DISCORD</h2>
  <p>
    <img src="https://img.shields.io/badge/Uses-TypeScript-2f74c1" alt="uses javascript">
    <img src="https://img.shields.io/badge/Uses-NodeJS-73ac61" alt="made with node.js">
    <img src="https://img.shields.io/badge/Uses-Sqlite-f7f7f7" alt="made with sqlite">
    <img src="https://img.shields.io/badge/NPM-Discord.js-1591f1" alt="uses discord.js">
    <img src="https://img.shields.io/badge/Use-Docker-blue" alt="uses Docker">
  </p>
</div>

## Installation

**[Docker](https://www.docker.com/) is required on your system**

File `.env` with correct values is required on project root (see `.env.model`)

### Development

#### Start

```shell
docker-compose up -d
```

## A propos / About

🇫🇷

Fonctionnalités :

- Gestion des arrivées et sorties avec l'attribution d'un rôle automatique
- Gestionnaire de tickets (demande d'assistance émise par les membres)
- Divers messages automatiques de modération
- Gestion des messages privés adressés au bot
- Gestion des administrateurs
- Envoi messages simples / enrichies / privés depuis le bot
- Gestion des rôles
- Création de sondages
- Utilise les commandes de l'application
- Récupère des informations sur le serveur de jeu via [Server Queries](https://developer.valvesoftware.com/wiki/Server_queries)
- Permet la création de salons vocaux privés pour les membres et fourni des outils d'administration

Utilise une base de données Sqlite et consomme l'API de [Top Serveurs](https://top-serveurs.net/).

Déployé sur une V.M OVH et actif sur un serveur [Discord](https://discord.gg/SMZJWyf).

🇬🇧/🇺🇸

Features :

- Management of arrivals and departures with the assignment of an automatic role
- Ticket manager (request for assistance from members)
- Various automatic moderation messages
- Management of private messages received
- Administrators manager
- Sending simple / enriched / private messages
- Role manager
- Creation of surveys
- Uses the application's commands
- Get information from game server with [Server Queries](https://developer.valvesoftware.com/wiki/Server_queries)
- Allows private vocal channel creation for members and provide administrator tools

Uses a Sqlite Database and consume the API of [Top Serveurs](https://top-serveurs.net/).

Deployed on a V.M OVH and active on [Discord](https://discord.gg/SMZJWyf).

![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)

---
