const path =
  process.env.NODE_ENV === 'production'
    ? './src/database/migrations/cli/umzug'
    : './dist/src/database/migrations/cli/umzug';
require(path).migrator.runAsCLI();
