#https://docs.gitlab.com/ee/ci/yaml/
image: node:20.12.2-alpine

stages:
  - cache
  - check
  - build-docker
  - deploy
  - migration

cache-modules:
  stage: cache
  interruptible: true
  script:
    - apk update
    - apk upgrade
    - apk add --no-cache
      make
      g++
      jpeg-dev
      cairo-dev
      giflib-dev
      pango-dev
      libtool
      autoconf
      automake
    - npm install
  cache:
    key: 'PIPELINE-$CI_PIPELINE_ID'
    policy: push
    paths:
      - node_modules/
  allow_failure: false
  except:
    - main

eslint:
  stage: check
  interruptible: true
  script:
    - npm run eslint
  cache:
    key: 'PIPELINE-$CI_PIPELINE_ID'
    policy: pull
    paths:
      - node_modules/
  allow_failure: false
  needs: ['cache-modules']
  except:
    - main

prettier:
  stage: check
  interruptible: true
  script:
    - npm run prettier
  cache:
    key: 'PIPELINE-$CI_PIPELINE_ID'
    policy: pull
    paths:
      - node_modules/
  allow_failure: false
  needs: ['cache-modules']
  except:
    - main

build:
  stage: check
  interruptible: true
  script:
    - npm run build
  cache:
    key: 'PIPELINE-$CI_PIPELINE_ID'
    policy: pull
    paths:
      - node_modules/
  allow_failure: false
  needs: ['cache-modules']
  except:
    - main

build-docker:
  stage: build-docker
  image: docker:23.0.6
  variables:
    DOCKER_TLS_CERTDIR: '/certs'
  services:
    - docker:23.0.6-dind
  script:
    - echo "$REGISTRY_PASSWORD" | docker login -u $REGISTRY_USER --password-stdin $REGISTRY_URL
    - docker build --tag $REGISTRY_IMAGE:$CI_COMMIT_REF_NAME --target production .
    - docker push $REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
    - docker logout $REGISTRY_URL
  allow_failure: false
  only:
    - main

.ssh-production:
  image: alpine:3
  before_script:
    - apk add -q --update --no-cache openssh-client
    - mkdir -p ~/.ssh
    - eval $(ssh-agent -s)
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - echo "$PROD_SSH_PRIVATE_KEY" | ssh-add -

deploy-production:
  extends: .ssh-production
  stage: deploy
  variables:
    TOKEN: $PROD_ENV_TOKEN
    DB_NAME: $PROD_ENV_DB_NAME
    ROLE_START: $PROD_ENV_ROLE_START
    ROLE_FRONTIERE: $PROD_ENV_ROLE_FRONTIERE
    CHA_WELCOME: $PROD_ENV_CHA_WELCOME
    CHA_RULES: $PROD_ENV_CHA_RULES
    CHA_FRONTIERE: $PROD_ENV_CHA_FRONTIERE
    CHA_DEPARTURE: $PROD_ENV_CHA_DEPARTURE
    DEV_ID: $PROD_ENV_DEV_ID
    TOKEN_TOP_SERVEUR: $PROD_ENV_TOKEN_TOP_SERVEUR
  script:
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "echo $REGISTRY_PASSWORD | docker login -u $REGISTRY_USER --password-stdin $REGISTRY_URL"
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker pull $REGISTRY_URL:$CI_COMMIT_REF_NAME"
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker logout $REGISTRY_URL"
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker stop bannis-bot-discord-prod || true && docker rm bannis-bot-discord-prod || true"
    - >
      ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker run -d \
        --env TOKEN=$TOKEN \
        --env DB_NAME=$DB_NAME \
        --env ROLE_START=$ROLE_START \
        --env ROLE_FRONTIERE=$ROLE_FRONTIERE \
        --env CHA_WELCOME=$CHA_WELCOME \
        --env CHA_RULES=$CHA_RULES \
        --env CHA_FRONTIERE=$CHA_FRONTIERE \
        --env CHA_DEPARTURE=$CHA_DEPARTURE \
        --env DEV_ID=$DEV_ID \
        --env TOKEN_TOP_SERVEUR=$TOKEN_TOP_SERVEUR \
        --mount type=volume,src=$PROD_VOLUME_DB_SRC,target=$PROD_VOLUME_DB_TARGET \
        --network=bannis-net \
        --name bannis-bot-discord-prod \
        --restart=always \
        $REGISTRY_URL:$CI_COMMIT_REF_NAME"
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker image prune -f"
  needs: ['build-docker']
  allow_failure: false
  environment:
    name: production
  only:
    - main

db-migration:
  extends: .ssh-production
  stage: migration
  script:
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker exec -ti bannis-bot-discord-prod npm run migration-up || true"
    - ssh -p $PROD_PORT $PROD_USER@$PROD_IP "docker restart bannis-bot-discord-prod || true"
  needs: ['deploy-production']
  allow_failure: false
  environment:
    name: production
  only:
    - main
  when: manual
