import * as dotenv from 'dotenv';
import { ColorResolvable, Snowflake } from 'discord.js';
import { LogService } from '../services/log.service';

dotenv.config();

export class Config {
  // NODE ENV
  public static readonly nodeEnvValues = {
    production: 'production',
    development: 'development',
    test: 'test',
  };
  public static readonly nodeEnv: string = process.env.NODE_ENV;

  // TOKENS
  public static readonly token: string = process.env.TOKEN;
  public static readonly tokenTopServer: string = process.env.TOKEN_TOP_SERVEUR;

  // CONFIG
  public static readonly color: ColorResolvable = [255, 255, 0];
  public static readonly devId: Snowflake = process.env.DEV_ID;
  public static readonly imageDir: string = './assets/images';
  public static readonly fontsDir: string = './assets/fonts';

  // CHANNELS
  public static readonly rulesChannelId: Snowflake = process.env.CHA_RULES;
  public static readonly welcomeChannel: Snowflake = process.env.CHA_WELCOME;
  public static readonly borderChannelId: string = process.env.CHA_FRONTIERE;
  public static readonly departureChannelId: string = process.env.CHA_DEPARTURE;

  // ROLES
  public static readonly roleStartId: Snowflake = process.env.ROLE_START;
  public static readonly roleFrontiereId: Snowflake = process.env.ROLE_FRONTIERE;

  // DATA BASE
  public static readonly DB = {
    name: process.env.DB_NAME,
  };

  public static async checkConfig() {
    if (!this.nodeEnv || !this.token || !this.roleFrontiereId || !this.roleStartId || !this.DB.name) {
      throw new Error("Variables d'environnement obligatoires absentes");
    }

    await LogService.info('Configuration OK');
  }
}
