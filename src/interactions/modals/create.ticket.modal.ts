import {
  ActionRowBuilder,
  ModalActionRowComponentBuilder,
  ModalBuilder,
  ModalSubmitInteraction,
  TextInputBuilder,
  TextInputStyle,
} from 'discord.js';
import { TicketService } from '../../services/ticket.service';
import { ModalInterface } from '../../interfaces/modal.interface';
import { ModalList } from '../../services/modal.service';

export class CreateTicketModal implements ModalInterface {
  public readonly customId = ModalList.CreateTicket;

  public getModalInstance(): ModalBuilder {
    return new ModalBuilder()
      .setCustomId(this.customId)
      .setTitle("On s'occupe de tout... No stress")
      .addComponents(
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
          new TextInputBuilder()
            .setCustomId('server-new-ticket')
            .setLabel('Serveur :')
            .setStyle(TextInputStyle.Short)
            .setRequired(true)
            .setPlaceholder('Sur quel serveur se situe le problème ?')
            .setMinLength(5)
            .setMaxLength(100)
        ),
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
          new TextInputBuilder()
            .setCustomId('time-new-ticket')
            .setLabel('Quand ?')
            .setStyle(TextInputStyle.Short)
            .setRequired(true)
            .setPlaceholder("A quelle heure s'est produit l'évènement ?")
            .setMinLength(2)
            .setMaxLength(100)
        ),
        new ActionRowBuilder<ModalActionRowComponentBuilder>().addComponents(
          new TextInputBuilder()
            .setCustomId('subject-new-ticket')
            .setLabel('Description :')
            .setStyle(TextInputStyle.Paragraph)
            .setRequired(true)
            .setPlaceholder("Merci d'être précis et concis afin de traiter au mieux ta demande...")
            .setMinLength(10)
        )
      );
  }

  public async execute(modalSubmitInteraction: ModalSubmitInteraction): Promise<void> {
    return TicketService.createTicket(modalSubmitInteraction);
  }
}
