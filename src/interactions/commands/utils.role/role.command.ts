import {
  ChatInputCommandInteraction,
  EmbedBuilder,
  GuildMember,
  PermissionFlagsBits,
  SlashCommandBuilder,
} from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { Config } from '../../../config/config';
import { InteractionError } from '../../../error/interaction.error';
import { LogService } from '../../../services/log.service';

export class RoleCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('role')
    .setDescription('Je peux ajouter ou supprimer des rôles pour un utilisateur')
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles)
    .addStringOption((option) =>
      option.setName('option').setDescription('Que veux-tu faire ?').setRequired(true).addChoices(
        {
          name: 'Ajouter',
          value: 'ajouter',
        },
        {
          name: 'Supprimer',
          value: 'supprimer',
        }
      )
    )
    .addUserOption((user) => user.setName('utilisateur').setDescription('Quel utilisateur ?').setRequired(true))
    .addRoleOption((role) =>
      role.setName('role').setDescription('Quel rôle ?').setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const targetMember = chatInputCommandInteraction.options.getMember('utilisateur') as GuildMember;
    const role = chatInputCommandInteraction.options.getRole('role');
    const option = chatInputCommandInteraction.options.getString('option');

    if (option === 'ajouter') {
      if (targetMember.roles.cache.has(role.id)) {
        throw new InteractionError(
          'Cet utilisateur a déjà ce rôle :unamused:',
          chatInputCommandInteraction.commandName,
          `Le membre ${targetMember.displayName} possede deja le role ${role.name}`
        );
      }

      await targetMember.roles.add(role.id);

      const dmMessageToUser = new EmbedBuilder()
        .setColor(Config.color)
        .setDescription(
          `:mailbox_with_mail: L'équipe des Bannis viens de t'attribuer le rôle de **\`\`${role.name}\`\`** :sunglasses:`
        );

      await targetMember.send({ embeds: [dmMessageToUser] });
      await chatInputCommandInteraction.editReply({
        content: "J'ai attribué le rôle et je l'ai prévenu par message privé :blush:",
      });
      return LogService.info(`Role ${role.name} ajoute a ${targetMember.displayName}`);
    }

    if (option === 'supprimer') {
      if (!targetMember.roles.cache.has(role.id)) {
        throw new InteractionError(
          'Cet utilisateur ne possède pas ce rôle :unamused:',
          chatInputCommandInteraction.commandName,
          `Le membre ${targetMember.displayName} ne possede pas le role ${role.name}`
        );
      }

      await targetMember.roles.remove(role.id);
      await chatInputCommandInteraction.editReply({ content: "J'ai bien supprimé le rôle :broom:" });
      return LogService.info(`Role ${role.name} supprime a ${targetMember.displayName}`);
    }
  }
}
