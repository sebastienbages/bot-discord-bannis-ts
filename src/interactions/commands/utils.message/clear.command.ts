import { ChatInputCommandInteraction, PermissionFlagsBits, SlashCommandBuilder, TextChannel } from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { InteractionError } from '../../../error/interaction.error';
import { LogService } from '../../../services/log.service';

export class ClearCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('effacer')
    .setDescription('Je peux effacer le nombre de message(s) que tu auras choisi dans le salon où tu te situe')
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages)
    .addNumberOption((numberMessage) =>
      numberMessage.setName('nombre').setDescription('Combien de messages ?').setMinValue(1).setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const numberOfMessages = chatInputCommandInteraction.options.getNumber('nombre');

    const channel = chatInputCommandInteraction.channel as TextChannel;
    const messagesDeleted = await channel.bulkDelete(numberOfMessages, true);

    if (messagesDeleted.size === 0) {
      throw new InteractionError(
        "J'ai supprimé ***0 message*** :grimacing: car aucun n'est plus récent que 15 jours",
        this.command.name,
        'Aucun message supprimé'
      );
    }

    if (messagesDeleted.size < numberOfMessages) {
      await chatInputCommandInteraction.editReply({
        content:
          `J'ai supprimé ***${messagesDeleted.size} message(s) sur ${numberOfMessages.toString()}*** :broom:` +
          '\n(les messages plus vieux de 15 jours ne peuvent pas être supprimés)',
      });
      return LogService.info(
        `${messagesDeleted.size} message(s) supprime sur ${numberOfMessages.toString()} dans le salon ${channel.name}`
      );
    }

    if (messagesDeleted.size === numberOfMessages) {
      await chatInputCommandInteraction.editReply({
        content: `J'ai supprimé ***${messagesDeleted.size} message(s)*** :broom:`,
      });
      return LogService.info(
        `${messagesDeleted.size} message(s) supprime sur ${numberOfMessages.toString()} dans le salon ${channel.name}`
      );
    }
  }
}
