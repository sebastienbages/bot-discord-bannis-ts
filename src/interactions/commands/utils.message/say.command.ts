import {
  ChannelType,
  ChatInputCommandInteraction,
  EmbedBuilder,
  PermissionFlagsBits,
  SlashCommandBuilder,
  TextChannel,
} from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { Config } from '../../../config/config';
import { InteractionError } from '../../../error/interaction.error';
import { LogService } from '../../../services/log.service';

export class SayCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('message')
    .setDescription('Je peux envoyer un message dans le channel où tu te situe')
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages)
    .addStringOption((style) =>
      style.setName('ecriture').setDescription("Quel style d'écriture ?").setRequired(true).addChoices(
        {
          name: 'Enrichi',
          value: 'enrichi',
        },
        {
          name: 'Normal',
          value: 'normal',
        }
      )
    )
    .addStringOption((message) =>
      message.setName('message').setDescription('Quel est ton message ?').setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ fetchReply: false });
    const textChannel = chatInputCommandInteraction.channel as TextChannel;

    if (textChannel.type !== ChannelType.GuildText) {
      throw new InteractionError(
        'Choisi un channel textuel voyons :grin:',
        chatInputCommandInteraction.commandName,
        `Le channel ${textChannel.name} ne possede pas le bon type`
      );
    }

    const option = chatInputCommandInteraction.options.getString('ecriture');
    const message = chatInputCommandInteraction.options.getString('message');

    if (option === 'enrichi') {
      const messageEmbed = new EmbedBuilder().setDescription(message).setColor(Config.color);
      await textChannel.send({ embeds: [messageEmbed] });
    }

    if (option === 'normal') {
      await textChannel.send({ content: message });
    }

    await chatInputCommandInteraction.deleteReply();
    return LogService.info(`Texte : "${message}" envoye dans le salon ${textChannel.name}`);
  }
}
