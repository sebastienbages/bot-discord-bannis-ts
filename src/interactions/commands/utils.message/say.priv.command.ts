import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { ChatInputCommandInteraction, GuildMember, PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { LogService } from '../../../services/log.service';

export class SayPrivCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('message-prive')
    .setDescription('Je peux envoyer un message privé à  un utilisateur')
    .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages)
    .addUserOption((user) => user.setName('utilisateur').setDescription('A quel utilisateur ?').setRequired(true))
    .addStringOption((message) =>
      message.setName('message').setDescription('Que est ton message ?').setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const message = chatInputCommandInteraction.options.getString('message');
    const guildMember = chatInputCommandInteraction.options.getMember('utilisateur') as GuildMember;
    await guildMember.send({ content: message });
    await chatInputCommandInteraction.editReply({
      content: 'Je lui ai bien envoyé le message en privé :shushing_face:',
    });
    return LogService.info(`Message : "${message}" - Envoye en prive a ${guildMember.displayName}`);
  }
}
