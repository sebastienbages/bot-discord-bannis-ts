import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  channelMention,
  ChannelType,
  ChatInputCommandInteraction,
  GuildMember,
  SlashCommandSubcommandBuilder,
  userMention,
  VoiceChannel,
} from 'discord.js';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';
import { getWrongPrivateVoiceChannelOwnerError } from '../../../error/private.voice.channel.feature.error';
import { InteractionError } from '../../../error/interaction.error';

export class GuestListSubcommand implements SubCommandInterface {
  public readonly name = 'liste-invites';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Recevoir la liste des invités de ton salon')
      .addChannelOption((channel) =>
        channel
          .setName('salon')
          .setDescription('Quel salon ?')
          .addChannelTypes(ChannelType.GuildVoice)
          .setRequired(true)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const channel: VoiceChannel = chatInputCommandInteraction.options.getChannel('salon');
    const guildMember = chatInputCommandInteraction.member as GuildMember;

    if (!(await PrivateVoiceChannelService.isOwner(guildMember.id, channel.id))) {
      throw getWrongPrivateVoiceChannelOwnerError(channel, guildMember);
    }

    const countGuests = await PrivateVoiceChannelService.countDiscordUserGuests(channel.id);

    if (countGuests === 0) {
      throw new InteractionError(
        `Il n'y a aucun invité dans ton salon ${channelMention(channel.id)} :face_with_monocle:`,
        'liste-invites',
        'aucun invite'
      );
    }

    let offset = 0;
    const limit = 20;
    while (countGuests > offset) {
      const guests = await PrivateVoiceChannelService.getPrivateVoiceChannelGuests(channel, limit, offset);
      await chatInputCommandInteraction.editReply({ content: guests.map((g) => userMention(g.id)).join(', ') });
      offset += limit;
    }
    await LogService.info(
      `Liste des invites du salon vocal prive ${channel.id} envoye au proprietaire ${chatInputCommandInteraction.user.id} dans la guild ${guildMember.guild.id}`
    );
  }
}
