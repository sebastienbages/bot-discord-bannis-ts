import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, inlineCode, SlashCommandSubcommandBuilder } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { AppError } from '../../../error/app.error';

export class TimeExpirationSubcommand implements SubCommandInterface {
  public readonly name = 'delai';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription("Modifier la durée de vie d'un salon vocal inactif")
      .addIntegerOption((option) =>
        option
          .setName('heures')
          .setDescription('Choisi une durée en HEURES (max 720h soit 30 jours)')
          .setRequired(true)
          .setMinValue(1)
          .setMaxValue(720)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const hours = chatInputCommandInteraction.options.getInteger('heures');
    const guild = chatInputCommandInteraction.guild;

    try {
      await PrivateVoiceChannelFeatureService.setTimeLimit(guild, hours);
      await chatInputCommandInteraction.editReply({
        content: `J'ai correctement modifié le délai d'expiration des salons sur ${inlineCode(hours.toString())} ${
          hours > 1 ? 'heures' : 'heure'
        } :ok_hand:`,
      });
      await LogService.info(
        `Delai expiration salon vocal prive modifie a ${hours.toString()} pour la guild ${guild.id}`
      );
    } catch (err) {
      if (err instanceof AppError) {
        throw new InteractionError(
          `Le délai est déjà de ${inlineCode(hours.toString())} heure(s) :face_with_raised_eyebrow:`,
          err.name,
          err.message
        );
      }
      throw err;
    }
  }
}
