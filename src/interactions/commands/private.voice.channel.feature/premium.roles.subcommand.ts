import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, Role, roleMention, SlashCommandSubcommandBuilder } from 'discord.js';
import { LogService } from '../../../services/log.service';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { InteractionError } from '../../../error/interaction.error';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';

export class PremiumRolesSubcommand implements SubCommandInterface {
  public readonly name = 'roles-premium';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Choisir le rôle premium sans durée de vie pour les salons privés')
      .addStringOption((option) =>
        option.setName('option').setDescription('Que veux-tu faire ?').setRequired(true).addChoices(
          {
            name: 'Ajouter le role',
            value: 'add-role',
          },
          {
            name: 'Supprimer le role',
            value: 'remove-role',
          }
        )
      )
      .addRoleOption((role) => role.setName('role').setDescription('Quel rôle ?').setRequired(true));
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const option = chatInputCommandInteraction.options.getString('option');
    const role = chatInputCommandInteraction.options.getRole('role') as Role;
    const guildId = chatInputCommandInteraction.guildId;
    switch (option) {
      case 'add-role':
        if (await PrivateVoiceChannelFeatureService.isPremiumRole(role)) {
          throw new InteractionError(
            `Le role ${roleMention(role.id)} existe déjà en tant que PREMIUM :face_with_raised_eyebrow:`,
            'addPremiumRole',
            `le role ${role.name} ${role.id} existe deja en tant que premium`
          );
        }

        await PrivateVoiceChannelFeatureService.addPremiumRole(role);
        await PrivateVoiceChannelService.addPremiumFlagToPrivateVoiceChannels(guildId, role);
        await LogService.info(`Role ${role.name} ${role.id} ajoute en tant que premium`);
        break;
      case 'remove-role':
        if (!(await PrivateVoiceChannelFeatureService.isPremiumRole(role))) {
          throw new InteractionError(
            `Le role ${roleMention(role.id)} n'existe pas en tant que PREMIUM :face_with_raised_eyebrow:`,
            'removePremiumRole',
            `le role ${role.name} ${role.id} n'existe pas en tant que PREMIUM`
          );
        }
        await PrivateVoiceChannelFeatureService.removePremiumRole(guildId);
        await PrivateVoiceChannelService.removePremiumFlagToPrivateVoiceChannels(guildId);
        await LogService.info(`Role ${role.name} ${role.id} supprime de premium`);
        break;
    }
    await chatInputCommandInteraction.editReply({
      content: `J'ai correctement ${option === 'add-role' ? 'ajouté' : 'supprimé'} le role ${roleMention(
        role.id
      )} en tant que PREMIUM :ok_hand:`,
    });
  }
}
