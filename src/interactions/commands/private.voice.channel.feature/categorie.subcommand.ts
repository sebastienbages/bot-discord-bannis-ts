import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { CategoryChannel, channelMention, ChannelType, ChatInputCommandInteraction } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { AppError } from '../../../error/app.error';

export class CategorieSubcommand implements SubCommandInterface {
  public readonly name = 'categorie';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Définir/Modifier la catégorie qui reçoit les salons vocaux')
      .addChannelOption((channel) =>
        channel
          .addChannelTypes(ChannelType.GuildCategory)
          .setName('categorie')
          .setDescription('Dans quel catégorie ?')
          .setRequired(true)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const category: CategoryChannel = chatInputCommandInteraction.options.getChannel('categorie');
    const guildId = chatInputCommandInteraction.guildId;

    try {
      await PrivateVoiceChannelFeatureService.saveCategory(category);
      await chatInputCommandInteraction.editReply({
        content: `La création des salons vocaux sera maintenant dans la catégorie ${channelMention(
          category.id
        )} :thumbsup:`,
      });
      await LogService.info(
        `Modification categorie private voice channel feature pour la guild ${guildId} : categorie ${category.name} ${category.id}`
      );
    } catch (err) {
      if (err instanceof AppError) {
        throw new InteractionError(
          `La categorie ${channelMention(category.id)} est déjà sélectionnée :face_with_raised_eyebrow:`,
          err.name,
          err.message
        );
      }
      throw err;
    }
  }
}
