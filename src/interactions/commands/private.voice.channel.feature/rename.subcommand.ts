import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  ChannelType,
  ChatInputCommandInteraction,
  GuildMember,
  SlashCommandSubcommandBuilder,
  VoiceChannel,
} from 'discord.js';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';
import { getWrongPrivateVoiceChannelOwnerError } from '../../../error/private.voice.channel.feature.error';

export class RenameSubcommand implements SubCommandInterface {
  public readonly name = 'renommer';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Renommer un salon vocal privé')
      .addChannelOption((channel) =>
        channel
          .setName('salon')
          .setDescription('Quel salon ?')
          .addChannelTypes(ChannelType.GuildVoice)
          .setRequired(true)
      )
      .addStringOption((option) =>
        option.setName('nom').setDescription('Quel nouveau nom ?').setMinLength(1).setMaxLength(100).setRequired(true)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const channel: VoiceChannel = chatInputCommandInteraction.options.getChannel('salon');
    const name = chatInputCommandInteraction.options.getString('nom');
    const guildMember = chatInputCommandInteraction.member as GuildMember;

    if (!(await PrivateVoiceChannelService.isOwner(guildMember.id, channel.id))) {
      throw getWrongPrivateVoiceChannelOwnerError(channel, guildMember);
    }

    await PrivateVoiceChannelService.renamePrivateVoiceChannel(channel, name);
    await chatInputCommandInteraction.editReply({
      content: 'Ton salon a été renommé :ok_hand:',
    });
    await LogService.info(
      `Channel vocal prive ${channel.name} ${channel.id} rennomme avec ${name} par user ${chatInputCommandInteraction.user.id}`
    );
  }
}
