import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { ChatInputCommandInteraction, PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { InteractionError } from '../../../error/interaction.error';
import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ActivateSubcommand } from './activate.subcommand';
import { CategorieSubcommand } from './categorie.subcommand';
import { MaxGuildChannelsSubcommand } from './max.guild.channels.subcommand';
import { MaxUserChannelsSubcommand } from './max.user.channels.subcommand';
import { PremiumRolesSubcommand } from './premium.roles.subcommand';
import { StateSubcommand } from './state.subcommand';
import { TimeExpirationSubcommand } from './time.expiration.subcommand';
import { VipRolesSubcommand } from './vip.roles.subcommand';

export class VocalAdminCommand implements SlashCommandInterface {
  public subCommandsMap: Map<string, SubCommandInterface>;
  public readonly command: SlashCommandBuilder;

  public constructor() {
    this.command = new SlashCommandBuilder()
      .setName('vocal-admin')
      .setDescription("Outils d'administration de la création de salons vocaux privés")
      .setDefaultMemberPermissions(PermissionFlagsBits.Administrator);
    this.subCommandsMap = new Map<string, SubCommandInterface>();
    [
      ActivateSubcommand,
      CategorieSubcommand,
      MaxGuildChannelsSubcommand,
      MaxUserChannelsSubcommand,
      PremiumRolesSubcommand,
      StateSubcommand,
      TimeExpirationSubcommand,
      VipRolesSubcommand,
    ].forEach((sc) => {
      const subCommand = new sc();
      this.subCommandsMap.set(subCommand.name, subCommand);
      this.command.addSubcommand(subCommand.subCommand());
    });
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ fetchReply: false, ephemeral: true });

    const subCommandName = chatInputCommandInteraction.options.getSubcommand();

    if (!this.subCommandsMap.has(subCommandName)) {
      throw new InteractionError(
        'Cette commande est introuvable :worried:\nMerci de le signaler à un administrateur',
        'vocal-admin',
        `commande ${subCommandName} introuvable`
      );
    }

    await this.subCommandsMap.get(subCommandName).execute(chatInputCommandInteraction);
  }
}
