import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  channelMention,
  ChatInputCommandInteraction,
  EmbedBuilder,
  roleMention,
  SlashCommandSubcommandBuilder,
} from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { InteractionError } from '../../../error/interaction.error';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';

export class StateSubcommand implements SubCommandInterface {
  public readonly name = 'etat';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription("Consulter l'état du système")
      .addStringOption((option) =>
        option.setName('option').setDescription('Que veux-tu consulter ?').setRequired(true).addChoices(
          {
            name: 'Statut de la fonctionnalité',
            value: 'status-voice-channel-feature',
          },
          {
            name: 'Récapitulatif des salons actifs',
            value: 'list-private-voice-channel',
          },
          {
            name: 'Liste des VIP',
            value: 'list-vip',
          }
        )
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const option = chatInputCommandInteraction.options.getString('option');
    const guildId = chatInputCommandInteraction.guildId;
    const limitRolesByQuery = 20;
    switch (option) {
      case 'status-voice-channel-feature':
        const privateVoiceChannelFeatureModel =
          await PrivateVoiceChannelFeatureService.getPrivateVoiceChannelFeature(guildId);
        const statusMessageEmbed = new EmbedBuilder()
          .setTitle('ETAT SYSTEME SALON VOCAL PRIVE')
          .setFields(
            {
              name: 'Power',
              value: privateVoiceChannelFeatureModel.isActive ? ':white_check_mark: activé' : ':x: désactivé',
            },
            {
              name: 'Catégorie des salons',
              value: privateVoiceChannelFeatureModel.categoryId
                ? channelMention(privateVoiceChannelFeatureModel.categoryId)
                : ':exclamation: Non défini',
            },
            {
              name: 'Nombre max de salons autorisé sur le serveur',
              value: privateVoiceChannelFeatureModel.maxVoiceChannelInGuild.toString(),
            },
            {
              name: 'Nombre max de salons autorisé par utilisateurs',
              value: privateVoiceChannelFeatureModel.maxVoiceChannelByUser.toString(),
            },
            {
              name: "Délai d'inactivité d'un salon",
              value: `${privateVoiceChannelFeatureModel.timeLimitInHours.toString()} heure(s)`,
            },
            {
              name: 'Role premium',
              value: `${
                privateVoiceChannelFeatureModel.premiumDiscordRoleId
                  ? roleMention(privateVoiceChannelFeatureModel.premiumDiscordRoleId)
                  : 'Non défini'
              }`,
            }
          )
          .setColor(privateVoiceChannelFeatureModel.isActive ? [75, 240, 8] : [255, 0, 0]);
        await chatInputCommandInteraction.editReply({ embeds: [statusMessageEmbed] });
        await LogService.info(
          `Status de private voice channel feature envoye a ${chatInputCommandInteraction.user.id} pour la guild ${chatInputCommandInteraction.guildId}`
        );
        break;
      case 'list-private-voice-channel':
        const countPrivateVoiceChannels = await PrivateVoiceChannelService.countPrivateVoiceChannelsForGuild(
          chatInputCommandInteraction.guildId
        );
        if (countPrivateVoiceChannels === 0) {
          throw new InteractionError(
            "Il n'existe aucun salon actif pour le moment :face_with_monocle:",
            'list-private-voice-channel',
            'aucun salon actif'
          );
        }
        const fileName = await PrivateVoiceChannelService.createPrivateVoiceChannelListFile(
          chatInputCommandInteraction.guildId
        );
        await chatInputCommandInteraction.user.send({
          files: [fileName],
          content: `Liste des salons vocaux privés pour la guild ${chatInputCommandInteraction.guild.name} ${chatInputCommandInteraction.guildId}`,
        });
        await chatInputCommandInteraction.editReply({ content: "Je t'ai envoyé la liste en privé :wink:" });
        await PrivateVoiceChannelService.deletePrivateVoiceChannelListFile(fileName);
        await LogService.info(
          `Liste des salon vocaux prives envoye a ${chatInputCommandInteraction.user.id} pour la guild ${chatInputCommandInteraction.guildId}`
        );
        break;
      case 'list-vip':
        const countVipRoles = await PrivateVoiceChannelFeatureService.countVipRoles(guildId);

        if (countVipRoles === 0) {
          throw new InteractionError("Il n'y a pas de rôle(s) VIP :face_with_monocle:", 'list-vip', 'pas de roles vip');
        }

        let vipOffset = 0;
        while (countVipRoles > vipOffset) {
          const vip = await PrivateVoiceChannelFeatureService.getVipRoles(guildId, limitRolesByQuery, vipOffset);
          vipOffset += limitRolesByQuery;
          await chatInputCommandInteraction.followUp({
            content: vip.map((r) => roleMention(r.id)).join(', '),
            ephemeral: true,
          });
        }
        await LogService.info(
          `Liste des vip de private voice channel feature envoye a ${chatInputCommandInteraction.user.id} pour la guild ${chatInputCommandInteraction.guildId}`
        );
        break;
    }
  }
}
