import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  ChannelType,
  ChatInputCommandInteraction,
  GuildMember,
  SlashCommandSubcommandBuilder,
  VoiceChannel,
} from 'discord.js';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';
import { getWrongPrivateVoiceChannelOwnerError } from '../../../error/private.voice.channel.feature.error';

export class RemoveSubcommand implements SubCommandInterface {
  public readonly name = 'supprimer';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Supprimer un salon vocal privé')
      .addChannelOption((channel) =>
        channel
          .setName('salon')
          .setDescription('Quel salon ?')
          .addChannelTypes(ChannelType.GuildVoice)
          .setRequired(true)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const channel: VoiceChannel = chatInputCommandInteraction.options.getChannel('salon');
    const guildMember = chatInputCommandInteraction.member as GuildMember;

    if (!(await PrivateVoiceChannelService.isOwner(guildMember.id, channel.id))) {
      throw getWrongPrivateVoiceChannelOwnerError(channel, guildMember);
    }

    await PrivateVoiceChannelService.removePrivateVoiceChannel(channel);
    await chatInputCommandInteraction.editReply({
      content: 'Ton salon a été supprimé :mute:',
    });
    await LogService.info(
      `Channel vocal prive ${channel.name} ${channel.id} supprime par user ${chatInputCommandInteraction.user.id}`
    );
  }
}
