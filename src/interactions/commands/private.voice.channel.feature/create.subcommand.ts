import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonStyle,
  channelLink,
  ChatInputCommandInteraction,
  GuildMember,
  SlashCommandSubcommandBuilder,
} from 'discord.js';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { InteractionError } from '../../../error/interaction.error';

export class CreateSubcommand implements SubCommandInterface {
  public readonly name: string = 'creer';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Créer un nouveau salon vocal privé')
      .addStringOption((option) =>
        option.setName('nom').setDescription('Choisi un nom pour ton salon').setMinLength(1).setMaxLength(100)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const name = chatInputCommandInteraction.options.getString('nom');
    const guildId = chatInputCommandInteraction.guildId;
    const guildMember = chatInputCommandInteraction.member as GuildMember;
    const countPrivateVoiceChannels = await PrivateVoiceChannelService.countPrivateVoiceChannelsForDiscordUser(
      chatInputCommandInteraction.guildId,
      guildMember.id
    );
    const maxPrivateVoiceChannelByUser =
      await PrivateVoiceChannelFeatureService.getMaxPrivateVoiceChannelByUser(guildId);

    if (countPrivateVoiceChannels >= maxPrivateVoiceChannelByUser) {
      throw new InteractionError(
        `Tu ne peux pas créer un nouveau salon car tu as atteint la limite autorisée par personne :face_with_monocle:`,
        'createPrivateVoiceChannel',
        `limite de creation atteinte de salon pour ${guildMember.displayName} ${guildMember.id}`
      );
    }

    const voiceChannelId = await PrivateVoiceChannelService.createPrivateVoiceChannel(
      chatInputCommandInteraction,
      name
    );
    const button = new ActionRowBuilder<ButtonBuilder>().addComponents(
      new ButtonBuilder()
        .setStyle(ButtonStyle.Link)
        .setLabel('Vers mon salon')
        .setURL(channelLink(voiceChannelId, guildId))
    );
    await chatInputCommandInteraction.editReply({
      content: `Ton salon est prêt et il n'attend plus que toi :loud_sound:`,
      components: [button],
    });
    await LogService.info(
      `Creation du salon vocal prive ${voiceChannelId} par user ${chatInputCommandInteraction.user.id} dans la guild ${guildId}`
    );
  }
}
