import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, inlineCode, SlashCommandSubcommandBuilder } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { AppError } from '../../../error/app.error';

export class MaxUserChannelsSubcommand implements SubCommandInterface {
  public readonly name = 'nbr-max-salons-utilisateur';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Modifier la limite du nombre de salon possédé par utilisateur')
      .addIntegerOption((option) =>
        option.setName('nombre').setDescription('Choisi un nombre').setRequired(true).setMinValue(1)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const commandValue = chatInputCommandInteraction.options.getInteger('nombre');
    const guildId = chatInputCommandInteraction.guildId;

    try {
      await PrivateVoiceChannelFeatureService.setMaxVoiceChannelByUser(guildId, commandValue);
      await chatInputCommandInteraction.editReply({
        content: `J'ai correctement modifié le nombre max de salons à ${inlineCode(
          commandValue.toString()
        )} par utilisateur :ok_hand:`,
      });
      await LogService.info(
        `Nbr max salons vocaux prive modifie a ${commandValue.toString()} pour chaque utilisateur pour la guild ${
          chatInputCommandInteraction.guildId
        }`
      );
    } catch (err) {
      if (err instanceof AppError) {
        throw new InteractionError(
          `La limite est déjà de ${inlineCode(commandValue.toString())} par utilisateur :face_with_raised_eyebrow:`,
          err.name,
          err.message
        );
      }
      throw err;
    }
  }
}
