import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, Role, roleMention, SlashCommandSubcommandBuilder } from 'discord.js';
import { LogService } from '../../../services/log.service';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { InteractionError } from '../../../error/interaction.error';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';

export class VipRolesSubcommand implements SubCommandInterface {
  public readonly name = 'roles-vip';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Choisir les roles qui ont un accès automatique à tous les salons')
      .addStringOption((option) =>
        option.setName('option').setDescription('Que veux-tu faire ?').setRequired(true).addChoices(
          {
            name: 'Ajouter un role',
            value: 'add-role',
          },
          {
            name: 'Supprimer un role',
            value: 'remove-role',
          }
        )
      )
      .addRoleOption((role) => role.setName('role').setDescription('Quel rôle ?').setRequired(true));
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const option = chatInputCommandInteraction.options.getString('option');
    const role = chatInputCommandInteraction.options.getRole('role') as Role;
    const guildId = chatInputCommandInteraction.guildId;
    switch (option) {
      case 'add-role':
        if (await PrivateVoiceChannelFeatureService.hasVipRole(role)) {
          throw new InteractionError(
            `Le role ${roleMention(role.id)} existe déjà en tant que VIP :face_with_raised_eyebrow:`,
            'addVipRole',
            `le role ${role.name} existe deja en tant que VIP`
          );
        }

        await PrivateVoiceChannelFeatureService.addVipRole(role);
        await PrivateVoiceChannelService.addVipRoleToExistingPrivateVoiceChannels(guildId, role);
        await LogService.info(`Role ${role.name} ${role.id} ajoute en tant que vip dans la guild ${guildId}`);
        break;
      case 'remove-role':
        if (!(await PrivateVoiceChannelFeatureService.hasVipRole(role))) {
          throw new InteractionError(
            `Le role ${roleMention(role.id)} n'existe pas en tant que VIP :face_with_raised_eyebrow:`,
            'removeVipRole',
            `le role ${role.name} n'existe pas en tant que VIP`
          );
        }
        await PrivateVoiceChannelFeatureService.removeVipRole(role);
        await PrivateVoiceChannelService.removeVipRoleToExistingPrivateVoiceChannels(guildId, role);
        await LogService.info(`Role ${role.name} ${role.id} supprime en tant que vip dans la guild ${guildId}`);
        break;
    }
    await chatInputCommandInteraction.editReply({
      content: `J'ai correctement ${option === 'add-role' ? 'ajouté' : 'supprimé'} le role ${roleMention(
        role.id
      )} en tant que VIP :ok_hand:`,
    });
  }
}
