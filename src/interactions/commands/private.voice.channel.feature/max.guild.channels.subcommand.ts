import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, inlineCode, SlashCommandSubcommandBuilder } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { AppError } from '../../../error/app.error';

export class MaxGuildChannelsSubcommand implements SubCommandInterface {
  public readonly name = 'nbr-max-salons-serveur';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Modifier la limite du nombre de salon créé sur ton serveur discord')
      .addIntegerOption((option) =>
        option.setName('nombre').setDescription('Choisi un nombre').setRequired(true).setMinValue(1)
      );
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const commandValue = chatInputCommandInteraction.options.getInteger('nombre');
    const guildId = chatInputCommandInteraction.guildId;

    try {
      await PrivateVoiceChannelFeatureService.setMaxVoiceChannelInGuild(guildId, commandValue);
      await chatInputCommandInteraction.editReply({
        content: `J'ai correctement modifié le nombre max de salons à ${inlineCode(
          commandValue.toString()
        )} pour le serveur :ok_hand:`,
      });
      await LogService.info(
        `Nbr max salons vocaux prive modifie a ${commandValue.toString()} pour la guild ${
          chatInputCommandInteraction.guildId
        }`
      );
    } catch (err) {
      if (err instanceof AppError) {
        throw new InteractionError(
          `La valeur de ${inlineCode(
            commandValue.toString()
          )} est identique a la limite actuelle :face_with_raised_eyebrow:`,
          err.name,
          err.message
        );
      }
      throw err;
    }
  }
}
