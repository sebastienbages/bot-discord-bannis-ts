import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { ChatInputCommandInteraction, inlineCode, SlashCommandSubcommandBuilder } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { AppError } from '../../../error/app.error';

export class ActivateSubcommand implements SubCommandInterface {
  public readonly name = 'on-off';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription('Activer ou désactiver la fonctionnalité');
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const guild = chatInputCommandInteraction.guild;

    try {
      const isActive = await PrivateVoiceChannelFeatureService.enableDisableFeature(guild);
      await chatInputCommandInteraction.editReply({
        content: `La création des salons vocaux est maintenant ${
          isActive ? 'activée :white_check_mark:' : 'désactivée :x:'
        }`,
      });
      await LogService.info(
        `Private voice channel feature ${isActive ? 'activee' : 'désactivee'} pour la guild ${guild.id}`
      );
    } catch (err) {
      if (err instanceof AppError) {
        throw new InteractionError(
          "Avant de pouvoir l'activer, tu dois définir une catégorie qui recevra les salons vocaux.\n" +
            `Utilise la commande ${inlineCode('/vocal-admin categorie')} :nerd:`,
          err.name,
          err.message
        );
      }
      throw err;
    }
  }
}
