import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import {
  channelMention,
  ChannelType,
  ChatInputCommandInteraction,
  GuildMember,
  PermissionFlagsBits,
  SlashCommandSubcommandBuilder,
  userMention,
  VoiceChannel,
} from 'discord.js';
import { PrivateVoiceChannelService } from '../../../services/private.voice.channel.service';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { getWrongPrivateVoiceChannelOwnerError } from '../../../error/private.voice.channel.feature.error';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { Readable } from 'node:stream';
import { DiscordRoleModel } from '../../../models/discord.role.model';

export class KickSubcommand implements SubCommandInterface {
  public readonly name = 'kick';

  public subCommand(): SlashCommandSubcommandBuilder {
    return new SlashCommandSubcommandBuilder()
      .setName(this.name)
      .setDescription("Déconnecte et supprime l'accès à un utilisateur pour ton salon vocal privé")
      .addChannelOption((channel) =>
        channel
          .setName('salon')
          .setDescription('Quel salon ?')
          .addChannelTypes(ChannelType.GuildVoice)
          .setRequired(true)
      )
      .addUserOption((user) => user.setName('utilisateur').setDescription('Quel utilisateur ?').setRequired(true));
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    const channel: VoiceChannel = chatInputCommandInteraction.options.getChannel('salon');
    const guest = chatInputCommandInteraction.options.getMember('utilisateur') as GuildMember;
    const guildMember = chatInputCommandInteraction.member as GuildMember;
    const guildId = guildMember.guild.id;

    if (!(await PrivateVoiceChannelService.isOwner(guildMember.id, channel.id))) {
      throw getWrongPrivateVoiceChannelOwnerError(channel, guildMember);
    }

    if (!(await PrivateVoiceChannelService.hasDiscordUserGuest(guest.id, channel.id))) {
      throw new InteractionError(
        "Ce membre n'est pas sur la liste des invités :face_with_monocle:",
        'removeGuestFromPrivateVoiceChannel',
        `Guild member ${guest.displayName} ${guest.id} doesn't exist in private voice channel ${channel.name} ${channel.id} in guild ${guildId}`
      );
    }

    if (guest.permissions.has(PermissionFlagsBits.Administrator)) {
      throw new InteractionError(
        'Tu ne peux pas kick un administrateur :smiling_imp:',
        'removeGuestFromPrivateVoiceChannel',
        `Kick not permitted, guild member ${guest.displayName} ${guest.id} is an admin in guild ${guildId}`
      );
    }

    const countVipRoles = await PrivateVoiceChannelFeatureService.countVipRoles(guildId);
    let offset = 0;
    const limit = 20;

    while (countVipRoles > offset) {
      const vipRoles = await PrivateVoiceChannelFeatureService.getVipRoles(guildId, limit, offset);
      const vipRolesStream = Readable.from(vipRoles);

      for await (const vipRole of vipRolesStream as unknown as DiscordRoleModel[]) {
        if (guest.roles.cache.has(vipRole.id)) {
          throw new InteractionError(
            'Tu ne peux pas kick un administrateur :smiling_imp:',
            'inviteUserToPrivateVoiceChannel',
            `Kick not permitted, guild member ${guest.displayName} ${guest.id} has vip role in guild ${guildId}`
          );
        }
      }

      offset += limit;
    }

    await PrivateVoiceChannelService.removeGuestFromPrivateVoiceChannel(channel, guest);
    await chatInputCommandInteraction.editReply({
      content: `${userMention(guest.id)} n'a plus accès à ton salon privé ${channelMention(channel.id)} :ok_hand:`,
    });
    await LogService.info(
      `Utilisateur ${guest.displayName} ${guest.id} kick du salon ${channel.name} ${channel.id} dans la guild ${guildId}`
    );
  }
}
