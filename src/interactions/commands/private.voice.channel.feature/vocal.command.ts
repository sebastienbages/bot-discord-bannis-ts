import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { ChatInputCommandInteraction, inlineCode, SlashCommandBuilder } from 'discord.js';
import { PrivateVoiceChannelFeatureService } from '../../../services/private.voice.channel.feature.service';
import { InteractionError } from '../../../error/interaction.error';
import { CreateSubcommand } from './create.subcommand';
import { SubCommandInterface } from '../../../interfaces/sub.command.interface';
import { RemoveSubcommand } from './remove.subcommand';
import { RenameSubcommand } from './rename.subcommand';
import { InviteSubcommand } from './invite.subcommand';
import { KickSubcommand } from './kick.subcommand';
import { GuestListSubcommand } from './guest.list.subcommand';

export class VocalCommand implements SlashCommandInterface {
  public subCommandsMap: Map<string, SubCommandInterface>;
  public command: SlashCommandBuilder;

  public constructor() {
    this.command = new SlashCommandBuilder().setName('vocal').setDescription('Commandes des salons vocaux privés');
    this.subCommandsMap = new Map<string, SubCommandInterface>();
    [
      CreateSubcommand,
      RemoveSubcommand,
      RenameSubcommand,
      InviteSubcommand,
      KickSubcommand,
      GuestListSubcommand,
    ].forEach((sc) => {
      const subCommand = new sc();
      this.subCommandsMap.set(subCommand.name, subCommand);
      this.command.addSubcommand(subCommand.subCommand());
    });
  }

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ fetchReply: false, ephemeral: true });
    const guildId = chatInputCommandInteraction.guildId;

    if (!(await PrivateVoiceChannelFeatureService.featureIsActive(guildId))) {
      throw new InteractionError(
        `Les commandes ${inlineCode('vocal')} sont désactivées pour le moment :sleeping:\nMerci de revenir plus tard`,
        'vocal-prive',
        'feature desactive dans les parametres'
      );
    }

    const subCommandName = chatInputCommandInteraction.options.getSubcommand();

    if (!this.subCommandsMap.has(subCommandName)) {
      throw new InteractionError(
        'Cette commande est introuvable :worried:\nMerci de le signaler à un administrateur',
        'vocal-prive',
        `commande ${subCommandName} introuvable`
      );
    }

    await this.subCommandsMap.get(subCommandName).execute(chatInputCommandInteraction);
  }
}
