import {
  AttachmentBuilder,
  ChannelType,
  ChatInputCommandInteraction,
  EmbedBuilder,
  PermissionFlagsBits,
  roleMention,
  SlashCommandBuilder,
  TextChannel,
} from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { Config } from '../../../config/config';
import { LogService } from '../../../services/log.service';

export class SurveyCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('sondage')
    .setDescription("Rédige ta question et j'enverrai un sondage le channel de ton choix")
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addStringOption((question) =>
      question.setName('question').setDescription('La question à poser...').setRequired(true)
    )
    .addChannelOption((channel) =>
      channel
        .setName('channel')
        .addChannelTypes(ChannelType.GuildText)
        .setDescription('Dans quel salon ?')
        .setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const sondageChannel: TextChannel = chatInputCommandInteraction.options.getChannel('channel');
    const question = chatInputCommandInteraction.options.getString('question');

    const image = new AttachmentBuilder(Config.imageDir + '/image-survey.png');
    const messageEmbed = new EmbedBuilder()
      .setTitle('SONDAGE')
      .setThumbnail('attachment://image-survey.png')
      .setDescription(`${roleMention(Config.roleStartId)} \n ${question}`)
      .setColor(Config.color);

    const survey = await sondageChannel.send({ embeds: [messageEmbed], files: [image] });
    await survey.react('👍');
    await survey.react('👎');

    await chatInputCommandInteraction.editReply({ content: "J'ai bien envoyé le sondage :blush:" });
    return LogService.info(`Sondage : "${question}" publie`);
  }
}
