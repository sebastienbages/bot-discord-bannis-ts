import { ChatInputCommandInteraction, GuildMember, PermissionFlagsBits } from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';
import { SlashCommandBuilder } from '@discordjs/builders';
import { Config } from '../../../config/config';

export class LogCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('log')
    .setDescription('Gestionnaire de mes logs')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addStringOption((option) =>
      option.setName('option').setDescription('Que veux-tu faire ?').setRequired(true).addChoices({
        name: 'Télécharger',
        value: 'download',
      })
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const guildMember = chatInputCommandInteraction.member as GuildMember;

    if (!guildMember.permissions.has(PermissionFlagsBits.Administrator)) {
      throw new InteractionError(
        'Tu dois être administrateur pour utiliser cette commande :smiling_imp:',
        this.command.name,
        'Command non accessible hors production'
      );
    }

    if (Config.nodeEnv !== Config.nodeEnvValues.production) {
      throw new InteractionError(
        "Cette commande n'est pas accessible hors production",
        this.command.name,
        'Command non accessible hors production'
      );
    }

    await guildMember.send({ files: [LogService.getLogFileName()] });
    await chatInputCommandInteraction.editReply({ content: 'Envoyé en message privé :ok_hand:' });
    return LogService.info(`Fichier des logs envoye a ${guildMember.displayName}`);
  }
}
