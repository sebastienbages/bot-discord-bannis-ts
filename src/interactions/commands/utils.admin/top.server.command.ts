import { ChatInputCommandInteraction, PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { TopServerService } from '../../../services/top.server.service';
import * as fs from 'node:fs/promises';
import { LogService } from '../../../services/log.service';
import { InteractionError } from '../../../error/interaction.error';

export class TopServerCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('top-serveur')
    .setDescription("Je peux t'envoyer le classement des votes sur Top serveur")
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addStringOption((option) =>
      option.setName('option').setDescription('Quel classement veux-tu ?').setRequired(true).addChoices(
        {
          name: 'Ce mois-ci',
          value: 'current_month',
        },
        {
          name: 'Le mois dernier',
          value: 'last_month',
        }
      )
    )
    .addStringOption((option) =>
      option
        .setName('filtrer-nom-joueur')
        .setDescription("Supprimer le nom d'un joueur")
        .setRequired(false)
        .setMinLength(3)
        .setMaxLength(30)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const option = chatInputCommandInteraction.options.getString('option');
    const playerName = chatInputCommandInteraction.options.getString('filtrer-nom-joueur') || '';

    if (option === 'current_month') {
      const playerRanking = await TopServerService.getPlayersRankingForCurrentMonth();

      if (playerRanking.players.length === 0) {
        throw new InteractionError(
          'La liste est vide :pleading_face:',
          'top-server-current-month',
          'la liste des votes top serveur du mois actuel est vide'
        );
      }

      const title = 'Classement Top Serveur du mois en cours';
      await TopServerService.createRankingFile(playerRanking.players, { playerName });
      await chatInputCommandInteraction.user.send({ files: [TopServerService.fileName], content: title });
      await LogService.info('Classement des votes du mois courant envoye');
    }

    if (option === 'last_month') {
      const playerRanking = await TopServerService.getPlayersRankingForLastMonth();

      if (playerRanking.players.length === 0) {
        throw new InteractionError(
          'La liste est vide :pleading_face:',
          'top-server-last-month',
          'la liste des votes top serveur du mois dernier est vide'
        );
      }

      const title = 'Classement Top Serveur du mois dernier';
      await TopServerService.createRankingFile(playerRanking.players, { playerName });
      await chatInputCommandInteraction.user.send({ files: [TopServerService.fileName], content: title });
      await LogService.info('Classement des votes du mois dernier envoye');
    }

    await chatInputCommandInteraction.editReply({ content: "Je t'ai envoyé le classement en privé :wink:" });
    await fs.rm(TopServerService.fileName);
    return LogService.info(`Fichier ${TopServerService.fileName} supprime`);
  }
}
