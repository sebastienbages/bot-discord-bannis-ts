import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import { ChannelType, ChatInputCommandInteraction, PermissionFlagsBits, SlashCommandBuilder } from 'discord.js';
import { TicketService } from '../../../services/ticket.service';

export class TicketCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('ticket')
    .setDescription('Envoyer le message pilote dans le channel où tu te situe')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addChannelOption((channel) =>
      channel
        .setName('channel')
        .addChannelTypes(ChannelType.GuildText)
        .setDescription('Dans quel salon ?')
        .setRequired(true)
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await TicketService.sendTicketMessage(chatInputCommandInteraction);
  }
}
