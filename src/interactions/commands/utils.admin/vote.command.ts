import { SlashCommandInterface } from '../../../interfaces/slash.command.interface';
import {
  ChannelType,
  ChatInputCommandInteraction,
  PermissionFlagsBits,
  SlashCommandBuilder,
  TextChannel,
} from 'discord.js';
import { VoteService } from '../../../services/vote.service';
import { LogService } from '../../../services/log.service';
import { Config } from '../../../config/config';
import { InteractionError } from '../../../error/interaction.error';

export class VoteCommand implements SlashCommandInterface {
  public readonly command = new SlashCommandBuilder()
    .setName('vote')
    .setDescription('Je sais gérer le message des votes pour Top Serveurs')
    .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
    .addSubcommand((subCmd) =>
      subCmd
        .setName('message')
        .setDescription('Je peux envoyer le message des votes')
        .addChannelOption((channel) =>
          channel
            .addChannelTypes(ChannelType.GuildText)
            .setName('channel')
            .setDescription('Dans quel salon ?')
            .setRequired(true)
        )
    )
    .addSubcommand((subCmd) =>
      subCmd
        .setName('activer-modifier')
        .setDescription('Je peux activer ou modifier le message automatique des votes')
        .addChannelOption((channel) =>
          channel
            .addChannelTypes(ChannelType.GuildText)
            .setName('channel')
            .setDescription('Dans quel Channel ?')
            .setRequired(true)
        )
        .addIntegerOption((timer) =>
          timer
            .setName('timer')
            .setDescription('Choisi un interval de temps en heure')
            .setRequired(true)
            .setMinValue(1)
            .setMaxValue(24)
        )
    )
    .addSubcommand((subCmd) =>
      subCmd.setName('desactiver').setDescription('Je peux désactiver le message automatique des votes')
    )
    .addSubcommand((subCmd) =>
      subCmd.setName('etat').setDescription("Je peux t'envoyer l'état actuel du message automatique des votes")
    ) as SlashCommandBuilder;

  public async execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true, fetchReply: false });

    if (!Config.tokenTopServer) {
      throw new InteractionError(
        'Je ne suis pas associé à un compte top serveur pour pouvoir continuer :worried:',
        'vote-command',
        'token top serveur absent'
      );
    }

    const subCommandName = chatInputCommandInteraction.options.getSubcommand();

    if (subCommandName === 'message') {
      const voteChannel: TextChannel = chatInputCommandInteraction.options.getChannel('channel');
      await VoteService.sendMessage(voteChannel, true);
      await chatInputCommandInteraction.editReply({ content: "J'ai bien envoyé le message :mechanical_arm:" });
      return LogService.info('Message des votes envoye');
    }

    if (subCommandName === 'activer-modifier') {
      await VoteService.activeVoteMessageAuto(chatInputCommandInteraction);
      return;
    }

    if (subCommandName === 'desactiver') {
      await VoteService.deactivateVoteMessageAuto();
      await chatInputCommandInteraction.editReply({
        content: "J'ai bien désactivé le message automatique des votes :mechanical_arm:",
      });
      return;
    }

    if (subCommandName === 'etat') {
      await VoteService.sendStatus(chatInputCommandInteraction);
      return;
    }
  }
}
