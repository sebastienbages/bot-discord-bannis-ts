import { ButtonInterface } from '../../interfaces/button.interface';
import { ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js';
import { RuleService } from '../../services/rule.service';
import { ButtonList } from '../../services/button.service';

export class ValidationRulesButton implements ButtonInterface {
  public readonly customId = ButtonList.ValidationRules;

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder()
      .setCustomId(this.customId)
      .setLabel("Valider le règlement et commencer l'aventure")
      .setStyle(ButtonStyle.Success)
      .setEmoji('🚀');
  }

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    return RuleService.validateRules(buttonInteraction);
  }
}
