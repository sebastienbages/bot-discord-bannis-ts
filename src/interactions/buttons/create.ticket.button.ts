import { ButtonInterface } from '../../interfaces/button.interface';
import {
  ActionRowBuilder,
  ButtonBuilder,
  ButtonInteraction,
  ButtonStyle,
  channelLink,
  GuildMember,
  roleMention,
  userMention,
} from 'discord.js';
import { ButtonList } from '../../services/button.service';
import { ModalList, ModalService } from '../../services/modal.service';
import { TicketService } from '../../services/ticket.service';
import { Config } from '../../config/config';
import { InteractionError } from '../../error/interaction.error';
import { Bot } from '../../bot';

export class CreateTicketButton implements ButtonInterface {
  public readonly customId = ButtonList.CreateTicket;

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder()
      .setCustomId(this.customId)
      .setLabel('Créer ticket')
      .setStyle(ButtonStyle.Primary)
      .setEmoji('🎫');
  }

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    const guildMember = buttonInteraction.member as GuildMember;
    const userTickets = await TicketService.getTicketsByUserId(guildMember.id);

    if (!guildMember.roles.cache.has(Config.roleStartId)) {
      if (Config.rulesChannelId) {
        const row = new ActionRowBuilder<ButtonBuilder>().addComponents(
          new ButtonBuilder()
            .setStyle(ButtonStyle.Link)
            .setLabel('Valider le règlement')
            .setURL(channelLink(Config.rulesChannelId, Bot.guildId))
        );
        await buttonInteraction.editReply({
          content: `Tu ne possède pas le rôle ${roleMention(
            Config.roleStartId
          )} :scream: \nUtilise ce bouton pour consulter le règlement et le valider :wink:`,
          components: [row],
        });
        return;
      } else {
        await buttonInteraction.editReply({
          content: `Tu ne possède pas le rôle ${roleMention(
            Config.roleStartId
          )} :scream: \nAdresse toi à un admin pour régler ça :wink:`,
        });
        return;
      }
    }

    if (userTickets.some((t) => t.isOpen)) {
      throw new InteractionError(
        `${userMention(
          guildMember.id
        )}, tu possèdes déjà un ticket ouvert, ferme le ou attend qu'il soit résolu :slight_smile:`,
        'createTicket',
        'ticket deja ouvert'
      );
    }

    await buttonInteraction.showModal(ModalService.getModal(ModalList.CreateTicket));
  }
}
