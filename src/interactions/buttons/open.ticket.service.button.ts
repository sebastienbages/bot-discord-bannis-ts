import { ButtonInterface } from '../../interfaces/button.interface';
import { ButtonList } from '../../services/button.service';
import { ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js';
import { TicketService } from '../../services/ticket.service';

export class OpenTicketServiceButton implements ButtonInterface {
  public readonly customId: ButtonList = ButtonList.OpenTicketService;

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    await TicketService.toggleTicketService(buttonInteraction);
  }

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder().setCustomId(this.customId).setStyle(ButtonStyle.Success).setEmoji('🔓');
  }
}
