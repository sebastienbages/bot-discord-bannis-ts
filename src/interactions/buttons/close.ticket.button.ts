import { ButtonInterface } from '../../interfaces/button.interface';
import { ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js';
import { TicketService } from '../../services/ticket.service';
import { ButtonList } from '../../services/button.service';

export class CloseTicketButton implements ButtonInterface {
  public readonly customId = ButtonList.CloseTicket;

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder()
      .setCustomId(this.customId)
      .setLabel('Fermer')
      .setStyle(ButtonStyle.Danger)
      .setEmoji('🔒');
  }

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    await TicketService.closeTicket(buttonInteraction);
  }
}
