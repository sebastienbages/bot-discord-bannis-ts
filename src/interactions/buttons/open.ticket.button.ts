import { ButtonInterface } from '../../interfaces/button.interface';
import { ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js';
import { TicketService } from '../../services/ticket.service';
import { ButtonList } from '../../services/button.service';

export class OpenTicketButton implements ButtonInterface {
  public readonly customId = ButtonList.OpenTicket;

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder()
      .setCustomId(this.customId)
      .setLabel('Ré-ouvrir')
      .setStyle(ButtonStyle.Success)
      .setEmoji('🔓');
  }

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    return TicketService.reOpenTicket(buttonInteraction);
  }
}
