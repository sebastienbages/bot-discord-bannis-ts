import { ButtonInterface } from '../../interfaces/button.interface';
import { ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js';
import { TicketService } from '../../services/ticket.service';
import { ButtonList } from '../../services/button.service';

export class DeleteTicketButton implements ButtonInterface {
  public readonly customId = ButtonList.DeleteTicket;

  public getButtonInstance(): ButtonBuilder {
    return new ButtonBuilder()
      .setCustomId(this.customId)
      .setLabel('Supprimer')
      .setStyle(ButtonStyle.Danger)
      .setEmoji('🧹');
  }

  public async execute(buttonInteraction: ButtonInteraction): Promise<void> {
    return TicketService.deleteTicket(buttonInteraction);
  }
}
