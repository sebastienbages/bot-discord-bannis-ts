import { StringSelectMenuBuilder, StringSelectMenuInteraction } from 'discord.js';

export interface SelectMenuInterface {
  readonly customId: string;
  getSelectMenuInstance(): StringSelectMenuBuilder;
  execute(selectMenuInteraction: StringSelectMenuInteraction): Promise<void>;
}
