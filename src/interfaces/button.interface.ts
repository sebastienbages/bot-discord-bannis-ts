import { ButtonBuilder, ButtonInteraction } from 'discord.js';
import { ButtonList } from '../services/button.service';

export interface ButtonInterface {
  readonly customId: ButtonList;
  getButtonInstance(): ButtonBuilder;
  execute(buttonInteraction: ButtonInteraction): Promise<void>;
}
