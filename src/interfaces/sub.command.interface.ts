import { SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { ChatInputCommandInteraction } from 'discord.js';

export interface SubCommandInterface {
  readonly name: string;
  subCommand(): SlashCommandSubcommandBuilder;
  execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void>;
}
