import { ChatInputCommandInteraction } from 'discord.js';
import { SlashCommandBuilder } from '@discordjs/builders';
import { SubCommandInterface } from './sub.command.interface';

export interface SlashCommandInterface {
  command: SlashCommandBuilder;
  subCommandsMap?: Map<string, SubCommandInterface>;
  execute(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void>;
}
