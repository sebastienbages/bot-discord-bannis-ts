import { ModalBuilder, ModalSubmitInteraction } from 'discord.js';
import { ModalList } from '../services/modal.service';

export interface ModalInterface {
  readonly customId: ModalList;
  getModalInstance(): ModalBuilder;
  execute(modalSubmitInteraction: ModalSubmitInteraction): Promise<void>;
}
