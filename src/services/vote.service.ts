import {
  ActionRowBuilder,
  AttachmentBuilder,
  ButtonBuilder,
  ButtonStyle,
  channelMention,
  ChatInputCommandInteraction,
  Client,
  EmbedBuilder,
  Message,
  TextChannel,
} from 'discord.js';
import { Config } from '../config/config';
import { VoteMessageModel } from '../models/vote.message.model';
import { TopServerService } from './top.server.service';
import { LogService } from './log.service';
import { Bot } from '../bot';
import { InteractionError } from '../error/interaction.error';
import { VoteConfigModel } from '../models/vote.config.model';

export class VoteService {
  private static messageAutoIsActive = false;
  private static intervalTimeAutoVoteMessage: number;
  private static timerAutoVoteMessage: NodeJS.Timeout;
  private static textChannelAutoVoteMessage: TextChannel;

  public static async initialize(client: Client): Promise<void> {
    const voteConfig = await VoteConfigModel.findByPk(Bot.guildId);

    if (voteConfig) {
      this.textChannelAutoVoteMessage = client.channels.cache.get(voteConfig.channelId) as TextChannel;
      this.intervalTimeAutoVoteMessage = voteConfig.timer;
      this.messageAutoIsActive = voteConfig.isActive;
    }

    if (voteConfig && voteConfig.isActive) {
      const timer = this.intervalTimeAutoVoteMessage * 60 * 60 * 1000;
      this.timerAutoVoteMessage = setInterval(() => {
        void this.sendMessage(this.textChannelAutoVoteMessage, false).then(() =>
          LogService.info('Message automatique des votes envoye')
        );
      }, timer);
      await LogService.info(
        `Message d'appel aux votes automatique active dans le channel ${
          this.textChannelAutoVoteMessage.name
        } toutes les ${this.intervalTimeAutoVoteMessage.toString()} heures`
      );
    }
  }

  /**
   * Envoi le message d'appel aux votes
   */
  public static async sendMessage(voteChannel: TextChannel, interaction = true): Promise<void> {
    try {
      const topServerInfos = await TopServerService.getSlugTopServer();
      const numberOfVotes = await TopServerService.getNumberOfVotes();
      const topServeurUrl = `https://top-serveurs.net/conan-exiles/${topServerInfos.server.slug}`;
      const logo = new AttachmentBuilder(`${Config.imageDir}/logo-topserver.png`);

      const messageEmbed = new EmbedBuilder()
        .setColor(Config.color)
        .setTitle('SOUTENEZ NOTRE COMMUNAUTE')
        .setURL(topServeurUrl)
        .setThumbnail('attachment://logo-topserver.png')
        .setDescription(
          "N'hésitez pas à donner un coup de pouce au serveur en votant sur Top Serveurs. " +
            'Merci pour votre soutien :thumbsup:'
        )
        .setFooter({ text: `Pour l'instant, nous avons ${numberOfVotes.toString()} votes ce mois-ci` });

      const components = new ActionRowBuilder<ButtonBuilder>().addComponents(
        new ButtonBuilder()
          .setStyle(ButtonStyle.Link)
          .setLabel('LIEN TOP SERVEURS')
          .setEmoji('🔗')
          .setURL(topServeurUrl)
      );

      const voteMessage = await voteChannel.send({ embeds: [messageEmbed], files: [logo], components: [components] });
      await this.saveMessage(voteMessage);
    } catch (error) {
      if (interaction) {
        throw error;
      }
      await LogService.handlerAppError(error as Error, voteChannel.client);
    }
  }

  /**
   * Active le message automatique d'appel aux votes
   *
   * @param chatInputCommandInteraction
   * @private
   */
  public static async activeVoteMessageAuto(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    this.intervalTimeAutoVoteMessage = chatInputCommandInteraction.options.getInteger('timer');
    this.textChannelAutoVoteMessage = chatInputCommandInteraction.options.getChannel('channel');
    const timer = this.intervalTimeAutoVoteMessage * 60 * 60 * 1000;
    this.timerAutoVoteMessage = setInterval(() => {
      void this.sendMessage(this.textChannelAutoVoteMessage, false).then(() =>
        LogService.info('Message automatique des votes envoye')
      );
    }, timer);

    const guildId = chatInputCommandInteraction.guildId;
    const channelId = this.textChannelAutoVoteMessage.id;
    const [affectedCount] = await VoteConfigModel.update(
      { guildId, channelId, isActive: true, timer: this.intervalTimeAutoVoteMessage, updatedAt: new Date() },
      { where: { guildId } }
    );

    if (affectedCount === 0) {
      await VoteConfigModel.create({
        guildId,
        channelId,
        isActive: true,
        timer: this.intervalTimeAutoVoteMessage,
        updatedAt: new Date(),
      });
    }

    if (this.messageAutoIsActive) {
      await chatInputCommandInteraction.editReply({
        content: `J'ai bien modifié le message automatique des votes qui s'affichera désormais dans le channel ${channelMention(
          this.textChannelAutoVoteMessage.id
        )} toutes les ${this.intervalTimeAutoVoteMessage.toString(10)} heure(s)`,
      });
      await LogService.info(
        `Message d'appel aux votes automatique modifie avec le channel ${
          this.textChannelAutoVoteMessage.name
        } toutes les ${this.intervalTimeAutoVoteMessage.toString()} heures`
      );
      return;
    }

    this.messageAutoIsActive = true;
    await chatInputCommandInteraction.editReply({
      content: `J'ai bien activé le message automatique des votes qui s'affichera dans le channel ${channelMention(
        this.textChannelAutoVoteMessage.id
      )} toutes les ${this.intervalTimeAutoVoteMessage.toString(10)} heure(s)`,
    });
    await LogService.info(
      `Message d'appel aux votes automatique active dans le channel ${
        this.textChannelAutoVoteMessage.name
      } toutes les ${this.intervalTimeAutoVoteMessage.toString()} heures`
    );
  }

  /**
   * Desactive le message automatique d'appel aux votes
   *
   * @private
   */
  public static async deactivateVoteMessageAuto(): Promise<void> {
    if (!this.messageAutoIsActive) {
      throw new InteractionError(
        'Le message auto est déjà désactivé :thumbsup:',
        'deactivateVoteMessageAuto',
        'Message auto des votes deja desactive'
      );
    }
    clearInterval(this.timerAutoVoteMessage);
    this.messageAutoIsActive = false;
    await VoteConfigModel.update({ isActive: false }, { where: { guildId: Bot.guildId } });
    await LogService.info("Message d'appel aux votes automatique desactive");
  }

  public static async sendStatus(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.editReply({
      content: `Le message automatique des votes est ${
        this.messageAutoIsActive
          ? `activé dans le channel ${channelMention(
              this.textChannelAutoVoteMessage.id
            )} toutes les ${this.intervalTimeAutoVoteMessage.toString(10)} heure(s)`
          : 'désactivé'
      }`,
    });
  }

  /**
   * Sauvegarde le message
   *
   * @param message {Message} - Message discord
   */
  private static async saveMessage(message: Message): Promise<void> {
    const lastMessage = await VoteMessageModel.findOne({ where: { guild: Bot.guildId } });
    if (lastMessage?.id) {
      await this.deleteLastDiscordMessage(lastMessage, message.channel as TextChannel);
      await VoteMessageModel.update({ id: message.id, channel: message.channel.id }, { where: { guild: Bot.guildId } });
      return;
    }
    await VoteMessageModel.create({ id: message.id, channel: message.channel.id, guild: Bot.guildId });
  }

  /**
   * Supprime le dernier message d'appel aux votes
   *
   * @param message
   * @param channel
   * @private
   */
  private static async deleteLastDiscordMessage(message: VoteMessageModel, channel: TextChannel): Promise<void> {
    try {
      const targetMessage = channel.messages.cache.get(message.id) || (await channel.messages.fetch(message.id));
      await targetMessage.delete();
    } catch (error) {
      await LogService.info('Precedent message introuvable');
    }
  }
}
