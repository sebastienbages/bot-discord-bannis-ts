import { DiscordRoleService } from './discord.role.service';
import { LogService } from './log.service';
import {
  ActionRowBuilder,
  Attachment,
  AttachmentBuilder,
  ButtonBuilder,
  ButtonInteraction,
  Collection,
  GuildMember,
  TextChannel,
} from 'discord.js';
import { Config } from '../config/config';
import { InteractionError } from '../error/interaction.error';
import { Stream } from 'node:stream';
import * as https from 'node:https';
import { ButtonList, ButtonService } from './button.service';
import { IncomingMessage } from 'node:http';

export class RuleService {
  private static readonly imagePattern = '[image]';

  /**
   * Valide le règlement pour l'émetteur de l'interaction
   *
   * @param buttonInteraction
   */
  public static async validateRules(buttonInteraction: ButtonInteraction): Promise<void> {
    await buttonInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const guildMember = buttonInteraction.member as GuildMember;

    if (DiscordRoleService.userHasRole(Config.roleStartId, guildMember)) {
      throw new InteractionError(
        'Tu as déjà validé le règlement :nerd:',
        buttonInteraction.customId,
        'Reglement deja valide'
      );
    }

    await DiscordRoleService.assignStartRole(buttonInteraction);
    await buttonInteraction.editReply({
      content: "Te voilà arrivé :partying_face: \nProfite bien de l'aventure des bannis :thumbsup:",
      attachments: [],
    });
    await LogService.info(`${guildMember.displayName} a commence l'aventure`);
  }

  /**
   * Traite le fichier du reglement et le publie dans le salon textuel fourni
   *
   * @param channel
   * @param attachements
   */
  public static async processRulesFile(
    channel: TextChannel,
    attachements: Collection<string, Attachment>
  ): Promise<void> {
    return new Promise((resolve, reject) => {
      try {
        const banner = new AttachmentBuilder(`${Config.imageDir}/bannis_accueil_epee.png`);
        void channel.send({ files: [banner] });

        const rulesFile = attachements.find(
          (a) => a.name.toLowerCase() === 'reglement.txt' || a.name.toLowerCase() === 'règlement.txt'
        );
        attachements.delete(rulesFile.id);

        const highWaterMark = 15 * 1024;
        let lastLineFragment = '';
        const transformStream = new Stream.Transform({
          objectMode: true,
          transform: (chunk: string, encoding, done) => {
            try {
              const str = `${lastLineFragment}${chunk.toString()}`;
              const lines = str.split(/\n|\r\n|\r/g);
              lastLineFragment = lines.pop() || '';
              done(null, lines);
            } catch (error) {
              done(error as Error);
            }
          },
          flush: (done) => {
            try {
              if (lastLineFragment !== '') {
                const line = [lastLineFragment.trim()];
                done(null, line);
              } else {
                done();
              }
            } catch (err) {
              done(err as Error);
            }
          },
        });

        const writeStream = new Stream.Writable({
          objectMode: true,
          highWaterMark,
          write: (chunk: string[], encoding, done) => {
            void (async () => {
              try {
                while (chunk.includes(RuleService.imagePattern) && attachements.size > 0) {
                  const index = chunk.indexOf(RuleService.imagePattern);
                  const remain = chunk.splice(index);
                  remain.shift();
                  const image = attachements.first();
                  const stream: IncomingMessage = await new Promise((resolveRequest) =>
                    https.get(image.url, (res) => {
                      resolveRequest(res);
                    })
                  );
                  await channel.send({ content: chunk.join('\n'), files: [stream] });
                  attachements.delete(image.id);
                  chunk = remain;
                }

                if (chunk.length > 0) {
                  await channel.send({ content: chunk.join('\n') });
                }

                done();
              } catch (error) {
                done(error as Error);
              }
            })();
          },
          final: (done) => {
            void (async () => {
              try {
                const validationButton = ButtonService.getButton(ButtonList.ValidationRules);
                const rowSelectMenu = new ActionRowBuilder<ButtonBuilder>().addComponents(validationButton);
                await channel.send({ components: [rowSelectMenu] });
              } catch (error) {
                done(error as Error);
              }
            })();
          },
        });

        https.get(rulesFile.url, (res) => {
          Stream.pipeline(res, transformStream, writeStream, (err) => {
            if (err) {
              return reject(err);
            }
            return resolve(LogService.info('Ecriture du reglement termine avec succes'));
          });
        });
      } catch (error) {
        return reject(error);
      }
    });
  }
}
