import { CreateTicketModal } from '../interactions/modals/create.ticket.modal';
import { Collection, ModalBuilder } from 'discord.js';
import { ModalInterface } from '../interfaces/modal.interface';
import { LogService } from './log.service';

export const enum ModalList {
  CreateTicket = 'create-ticket-modal',
}

export class ModalService {
  private static readonly modals = [CreateTicketModal];

  private static readonly modalsInstanceCollection = new Collection<string, ModalInterface>();

  public static async initialize() {
    this.modals.forEach((modalClass) => {
      const instance = new modalClass() as ModalInterface;
      this.modalsInstanceCollection.set(instance.customId, instance);
    });
    await LogService.info('Modal service OK');
  }

  /**
   * Retourne l'instance de la modal selon son Id
   *
   * @param customId
   */
  public static getInstance(customId: string | ModalList): ModalInterface {
    return this.modalsInstanceCollection.get(customId);
  }

  /**
   * Retourne une nouvelle modale selon son Id
   *
   * @param customId
   */
  public static getModal(customId: string | ModalList): ModalBuilder {
    return this.modalsInstanceCollection.get(customId).getModalInstance();
  }
}
