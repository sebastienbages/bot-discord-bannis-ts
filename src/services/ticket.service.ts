import {
  ActionRowBuilder,
  AttachmentBuilder,
  ButtonBuilder,
  ButtonInteraction,
  ButtonStyle,
  CategoryChannel,
  ChannelType,
  ChatInputCommandInteraction,
  EmbedBuilder,
  GuildMember,
  HexColorString,
  messageLink,
  ModalSubmitInteraction,
  PermissionFlagsBits,
  Role,
  roleMention,
  TextChannel,
  userMention,
} from 'discord.js';
import { Config } from '../config/config';
import { TicketModel } from '../models/ticket.model';
import { LogService } from './log.service';
import { InteractionError } from '../error/interaction.error';
import { ButtonList, ButtonService } from './button.service';
import util from 'util';
import { Bot } from '../bot';

interface TimeLeftType {
  minutes: string;
  seconds: string;
}

export class TicketService {
  private static delayIsActive = false;
  private static readonly cooldownDuration: number = 10 * 60 * 1000;
  private static readonly datesCooldown: Date[] = [];
  private static requests = 0;
  private static readonly warningColor = 'FF0000' as HexColorString;

  /**
   * Retourne un ticket selon l'utilisateur
   *
   * @param userId {string} - Utilisateur discord
   */
  public static async getTicketsByUserId(userId: string): Promise<TicketModel[]> {
    return TicketModel.findAll({ where: { userId, guildId: Bot.guildId } });
  }

  /**
   * Création d'un nouveau ticket
   *
   * @param interaction
   */
  public static async createTicket(interaction: ModalSubmitInteraction): Promise<void> {
    await interaction.deferReply({ ephemeral: true, fetchReply: false });
    const guildMember = interaction.member as GuildMember;
    const guild = interaction.guild;

    const category = interaction.channel.parent as CategoryChannel;
    const everyoneRole: Role = guild.roles.cache.find((r) => r.name === '@everyone');
    let lastNumberTicket = await TicketModel.max<number, TicketModel>('count');
    if (!category || !everyoneRole || lastNumberTicket === 9999) {
      throw new InteractionError(
        'Il semble que la création des tickets soit indisponible :weary:',
        'create-ticket',
        'paramètres'
      );
    }
    if (!lastNumberTicket) {
      lastNumberTicket = 0;
    }

    const ticketNumber = ++lastNumberTicket;
    const ticketChannel: TextChannel = await guild.channels.create<ChannelType.GuildText>({
      type: ChannelType.GuildText,
      parent: category,
      name: `ticket-${ticketNumber.toString().padStart(4, '0')}`,
      permissionOverwrites: [
        {
          id: guildMember.id,
          allow: [PermissionFlagsBits.ViewChannel],
          deny: [PermissionFlagsBits.AddReactions],
        },
        {
          id: everyoneRole.id,
          deny: [PermissionFlagsBits.ViewChannel],
        },
      ],
    });

    const ticketRoles = guild.roles.cache.filter(
      (r) =>
        (r.permissions.has(PermissionFlagsBits.Administrator) ||
          r.permissions.has(PermissionFlagsBits.ManageMessages)) &&
        !r.managed &&
        r.members.size > 0
    );
    const rolesMentions = [];
    await Promise.all(
      ticketRoles.map((r) => {
        rolesMentions.push(roleMention(r.id));
        return ticketChannel.permissionOverwrites.create(r, {
          AddReactions: false,
          ViewChannel: true,
        });
      })
    );
    rolesMentions.push(userMention(guildMember.id));
    await ticketChannel.send(rolesMentions.join(' '));

    const closeTicketButton = ButtonService.getButton(ButtonList.CloseTicket);
    const ticketRow = new ActionRowBuilder<ButtonBuilder>().addComponents(closeTicketButton);

    const subject = interaction.fields.getTextInputValue('subject-new-ticket');
    const server = interaction.fields.getTextInputValue('server-new-ticket');
    const when = interaction.fields.getTextInputValue('time-new-ticket');

    const ticket: EmbedBuilder = new EmbedBuilder()
      .setColor(Config.color)
      .setTitle(`TICKET N°${ticketNumber}`)
      .setDescription(subject)
      .setFields({ name: 'Où ?', value: server, inline: true }, { name: 'Quand ?', value: when, inline: true })
      .setThumbnail('attachment://ticket.png')
      .setAuthor({ name: guildMember.displayName, iconURL: guildMember.user.avatarURL() })
      .setTimestamp()
      .setFooter({
        text: 'Une fois résolu, tu peux fermer le ticket avec le bouton ci-dessous',
      });

    const ticketImage = new AttachmentBuilder(`${Config.imageDir}/ticket.png`);
    const welcomeMessage = await ticketChannel.send({
      embeds: [ticket],
      components: [ticketRow],
      files: [ticketImage],
    });

    await TicketModel.create({
      userId: guildMember.id,
      count: ticketNumber,
      subject,
      server,
      when,
      guildId: Bot.guildId,
    });

    const responseRow = new ActionRowBuilder<ButtonBuilder>().addComponents(
      new ButtonBuilder()
        .setStyle(ButtonStyle.Link)
        .setLabel('Vers mon ticket')
        .setURL(messageLink(ticketChannel.id, welcomeMessage.id, Bot.guildId))
    );
    await interaction.editReply({
      content: "Ton ticket est prêt et il n'attend plus que toi :ok_hand:",
      components: [responseRow],
    });

    return LogService.info(`Création du ticket N°${ticketNumber.toString()} pour ${guildMember.displayName}`);
  }

  /**
   * Fermeture d'un ticket
   *
   * @param buttonInteraction
   */
  public static async closeTicket(buttonInteraction: ButtonInteraction): Promise<void> {
    await buttonInteraction.deferReply({ fetchReply: false });
    const message = buttonInteraction.message;

    const closeTicketButton = ButtonService.getButton(ButtonList.CloseTicket).setDisabled(true);
    const newActionRow = new ActionRowBuilder<ButtonBuilder>().addComponents(closeTicketButton);
    await message.edit({ components: [newActionRow] });

    const targetChannel = buttonInteraction.channel as TextChannel;
    const userTicket = await this.getTicketByNumber(targetChannel);
    await targetChannel.permissionOverwrites.edit(userTicket.userId, {
      ViewChannel: false,
      SendMessages: false,
    });

    const guildMember = buttonInteraction.member as GuildMember;
    const closeMessage: EmbedBuilder = new EmbedBuilder()
      .setColor(Config.color)
      .setDescription(`Ticket fermé par ${userMention(guildMember.id)}`);

    const openTicketButton = ButtonService.getButton(ButtonList.OpenTicket);
    const deleteTicketButton = ButtonService.getButton(ButtonList.DeleteTicket);
    const row = new ActionRowBuilder<ButtonBuilder>().addComponents(openTicketButton, deleteTicketButton);

    await buttonInteraction.editReply({ embeds: [closeMessage], components: [row] });

    if (!this.delayIsActive) {
      this.addRequest();
      const newName: string = targetChannel.name.replace('ticket', 'fermé');
      await targetChannel.setName(newName);
    } else {
      await this.sendCooldownMessage(targetChannel);
    }

    await TicketModel.update(
      { isOpen: false },
      { where: { count: userTicket.count, userId: userTicket.userId, guildId: Bot.guildId } }
    );
    return LogService.info(
      `Fermeture d'un ticket : N°${userTicket.count} par le membre ${guildMember.displayName} (${guildMember.id})`
    );
  }

  /**
   * Ré-ouverture d'un ticket
   *
   * @param buttonInteraction
   */
  public static async reOpenTicket(buttonInteraction: ButtonInteraction): Promise<void> {
    await buttonInteraction.deferReply({ fetchReply: false });

    const targetChannel = buttonInteraction.channel as TextChannel;
    const userTicket = await this.getTicketByNumber(targetChannel);
    const message = buttonInteraction.message;
    await message.delete();

    const guildTicketMember = buttonInteraction.guild.members.cache.get(userTicket.userId);
    if (guildTicketMember) {
      await targetChannel.permissionOverwrites.edit(userTicket.userId, {
        ViewChannel: true,
        SendMessages: true,
      });
    }

    if (!this.delayIsActive) {
      this.addRequest();
      const newName: string = targetChannel.name.replace('fermé', 'ticket');
      await targetChannel.setName(newName);
    } else {
      await this.sendCooldownMessage(targetChannel);
    }

    const closeTicketButton = ButtonService.getButton(ButtonList.CloseTicket);
    const row = new ActionRowBuilder<ButtonBuilder>().addComponents(closeTicketButton);
    await buttonInteraction.editReply({
      content: guildTicketMember
        ? `Hey ${userMention(userTicket.userId)}, ton ticket a été ré-ouvert :face_with_monocle:`
        : "L'auteur a quitté le discord :worried:",
      components: [row],
    });

    await TicketModel.update(
      { isOpen: true },
      { where: { count: userTicket.count, userId: userTicket.userId, guildId: Bot.guildId } }
    );
    return LogService.info(`Ré-ouverture d'un ticket : N°${userTicket.count}`);
  }

  /**
   * Supprimer un ticket
   *
   * @param buttonInteraction
   */
  public static async deleteTicket(buttonInteraction: ButtonInteraction): Promise<void> {
    await buttonInteraction.deferReply({ fetchReply: false });
    const message = buttonInteraction.message;

    const openTicketButton = ButtonService.getButton(ButtonList.OpenTicket).setDisabled(true);
    const deleteTicketButton = ButtonService.getButton(ButtonList.DeleteTicket).setDisabled(true);
    const newActionRow = new ActionRowBuilder<ButtonBuilder>().addComponents(openTicketButton, deleteTicketButton);
    await message.edit({ components: [newActionRow] });

    const deleteMessage: EmbedBuilder = new EmbedBuilder()
      .setColor(this.warningColor)
      .setDescription('Suppression du ticket dans quelques secondes');
    await buttonInteraction.editReply({ embeds: [deleteMessage] });

    const targetChannel = buttonInteraction.channel as TextChannel;
    const wait = util.promisify(setTimeout);
    await wait(5000);
    await targetChannel.delete();
    const userTicket = await this.getTicketByNumber(targetChannel);
    await TicketModel.update(
      { isDeleted: true },
      { where: { count: userTicket.count, userId: userTicket.userId, guildId: Bot.guildId } }
    );
    return LogService.info(`Suppression d'un ticket : ${targetChannel.name}`);
  }

  /**
   * Envoi le message pilote pour la création des tickets
   *
   * @param chatInputCommandInteraction
   */
  public static async sendTicketMessage(chatInputCommandInteraction: ChatInputCommandInteraction): Promise<void> {
    await chatInputCommandInteraction.deferReply({ ephemeral: true });
    const channel: TextChannel = chatInputCommandInteraction.options.getChannel('channel');

    const embedMessage = new EmbedBuilder()
      .setColor(Config.color)
      .setThumbnail(chatInputCommandInteraction.guild.iconURL())
      .setTitle("BESOIN D'AIDE ?")
      .setDescription(
        'Rien de plus simple, utilise le bouton ci-dessous pour créer ton ticket. \n \n' +
          "Un salon sera créé rien que pour toi afin de discuter avec l'équipe des Bannis :wink: \n"
      )
      .setFooter({ text: 'Un seul ticket autorisé par utilisateur' });

    const createTicketButton = ButtonService.getButton(ButtonList.CreateTicket);
    const closeTicketServiceButton = ButtonService.getButton(ButtonList.CloseTicketService);
    const row = new ActionRowBuilder<ButtonBuilder>().addComponents(createTicketButton, closeTicketServiceButton);
    await channel.send({ embeds: [embedMessage], components: [row] });
    await chatInputCommandInteraction.editReply({ content: "J'ai bien envoyé le message :ticket:" });
    return LogService.info(`Message ticket pilote envoye dans le salon ${channel.name}`);
  }

  /**
   * Ouvre/ferme le système de ticket
   *
   * @param buttonInteraction
   */
  public static async toggleTicketService(buttonInteraction: ButtonInteraction): Promise<void> {
    await buttonInteraction.deferReply({ ephemeral: true, fetchReply: false });
    const message = buttonInteraction.message;
    const guildMember = buttonInteraction.member as GuildMember;

    if (!guildMember.permissions.has(PermissionFlagsBits.Administrator)) {
      throw new InteractionError(
        'Seul un administrateur à ce pouvoir :smiling_imp:',
        'toggleTicketService',
        buttonInteraction.customId === ButtonList.OpenTicketService.toString()
          ? `${guildMember.displayName} a essayé d'ouvrir le ticket service`
          : `${guildMember.displayName} a essayé de fermer le ticket service`
      );
    }

    if (buttonInteraction.customId === ButtonList.OpenTicketService.toString()) {
      const createTicketButton = ButtonService.getButton(ButtonList.CreateTicket);
      const closeTicketServiceButton = ButtonService.getButton(ButtonList.CloseTicketService);
      const actionRow = new ActionRowBuilder<ButtonBuilder>().addComponents(
        createTicketButton,
        closeTicketServiceButton
      );
      await message.edit({ components: [actionRow] });
      await buttonInteraction.editReply({ content: 'La création de ticket est ouverte 🔓' });
      return;
    }

    if (buttonInteraction.customId === ButtonList.CloseTicketService.toString()) {
      const createTicketButton = ButtonService.getButton(ButtonList.CreateTicket)
        .setDisabled(true)
        .setLabel('Système en maintenance...');
      const openTicketServiceButton = ButtonService.getButton(ButtonList.OpenTicketService);
      const actionRow = new ActionRowBuilder<ButtonBuilder>().addComponents(
        createTicketButton,
        openTicketServiceButton
      );
      await message.edit({ components: [actionRow] });
      await buttonInteraction.editReply({ content: 'La création de ticket est désactivée 🔒' });
      return;
    }

    throw new Error('Toggle ticket service échoué');
  }

  /**
   * Retourne un ticket selon son numéro
   *
   * @param targetChannel {TextChannel} - Salon textuel discord du ticket
   */
  private static async getTicketByNumber(targetChannel: TextChannel): Promise<TicketModel> {
    const nameArray: string[] = targetChannel.name.split('-');
    const ticketNumber: number = parseInt(nameArray[1], 10);
    return TicketModel.findOne<TicketModel>({ where: { count: ticketNumber, guildId: Bot.guildId } });
  }

  /**
   * Envoi le message d'information de bloquage des requêtes de tickets
   *
   * @param targetChannel
   * @private
   */
  private static async sendCooldownMessage(targetChannel: TextChannel): Promise<void> {
    const now: number = new Date().getTime();
    const lastCooldown = this.datesCooldown[0].getTime();
    const differenceMs = now - lastCooldown;
    let minutes: number = Math.floor((this.cooldownDuration - differenceMs) / 60);
    let seconds: number = Math.floor((this.cooldownDuration - differenceMs) / 1000);
    minutes = minutes % 60;
    seconds = seconds % 60;
    const timeLeft = {
      minutes: minutes.toString().padStart(2, '0'),
      seconds: seconds.toString().padStart(2, '0'),
    } as TimeLeftType;
    const message: EmbedBuilder = new EmbedBuilder()
      .setDescription(':exclamation: Le ticket ne peut pas changer de nom trop souvent')
      .setFooter({
        text: `Cela sera à nouveau possible dans ${timeLeft.minutes} minute(s) et ${timeLeft.seconds} seconde(s)`,
      })
      .setColor(this.warningColor);

    await targetChannel.send({ embeds: [message] });
  }

  /**
   * Ajoute une requête ticket au compteur, initialise et démarre son minuteur de recharge
   *
   * @private
   */
  private static addRequest(): void {
    if (this.requests < 2) {
      this.datesCooldown.push(new Date());

      setTimeout(() => {
        this.requests--;
        this.datesCooldown.shift();
        if (this.requests < 2) {
          this.delayIsActive = false;
        }
      }, this.cooldownDuration);

      this.requests++;
      if (this.requests === 2) {
        this.delayIsActive = true;
      }
    }
  }
}
