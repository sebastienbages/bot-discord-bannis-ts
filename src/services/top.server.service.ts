import { TopServerModel } from '../models/top.server.model';
import * as fs from 'node:fs/promises';
import { TopServerMember, TopServerPlayerRanking, TopServerInfos } from '../models/top.server.model';
import * as Stream from 'node:stream';

export interface RankingFileFilters {
  playerName?: string;
}

export class TopServerService {
  public static readonly fileName = 'topserveur.txt';

  /**
   * Retourne le nom du serveur sous Top Serveur
   */
  public static async getSlugTopServer(): Promise<TopServerInfos> {
    return TopServerModel.getDataServer();
  }

  /**
   * Retourne la liste des votants du mois courant et le nombre de votes
   */
  public static async getPlayersRankingForCurrentMonth(): Promise<TopServerPlayerRanking> {
    return TopServerModel.getPlayersRankingForCurrentMonth();
  }

  /**
   * Retourne la liste des votants du mois courant et le nombre de votes
   */
  public static async getPlayersRankingForLastMonth(): Promise<TopServerPlayerRanking> {
    return TopServerModel.getPlayersRankingForLastMonth();
  }

  /**
   * Retourne le total des votes pour le mois courant
   *
   * @constructor
   */
  public static async getNumberOfVotes(): Promise<number> {
    const serverStats = await TopServerModel.getServerStats();
    const date = new Date();
    const stats = serverStats.stats.monthly.find((s) => s.year === date.getFullYear());
    const months: string[] = [
      'january',
      'february',
      'march',
      'april',
      'may',
      'june',
      'july',
      'august',
      'september',
      'october',
      'november',
      'december',
    ];
    const currentMonth: string = months[date.getMonth()];
    return stats[currentMonth + '_votes'] as number;
  }

  /**
   * Créer un fichier avec le classement des votes
   *
   * @param topServerMembers {TopServerMember[]}
   * @param filters
   */
  public static async createRankingFile(
    topServerMembers: TopServerMember[],
    filters: RankingFileFilters
  ): Promise<void> {
    return new Promise((resolve, reject) => {
      const filteredMembers = topServerMembers.filter(
        (member) =>
          member.playername !== '' && member.playername.toLowerCase() !== filters.playerName.trim().toLowerCase()
      );

      let rank = 1;
      const reader = Stream.Readable.from(filteredMembers);
      const writer = new Stream.Writable({
        objectMode: true,
        write: (player: TopServerMember, encoding: BufferEncoding, done: (error?: Error | null) => void) => {
          void (async () => {
            try {
              await fs.appendFile(
                TopServerService.fileName,
                `${rank.toString()} - ${player.playername} - ${player.votes} votes \n`
              );
              rank += 1;
              done();
            } catch (err) {
              done(err as Error);
            }
          })();
        },
      });

      Stream.pipeline(reader, writer, (err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }
}
