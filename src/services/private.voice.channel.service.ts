import {
  ChannelType,
  ChatInputCommandInteraction,
  GuildMember,
  PermissionFlagsBits,
  Role,
  Snowflake,
  VoiceChannel,
} from 'discord.js';
import { PrivateVoiceChannelModel } from '../models/private.voice.channel.model';
import fsPromises from 'node:fs/promises';
import { DiscordUserModel } from '../models/discord.user.model';
import { SequelizeClient } from '../database/client/sequelize.client';
import { PrivateVoiceChannelFeatureService } from './private.voice.channel.feature.service';
import { DiscordUserService } from './discord.user.service';
import * as fs from 'node:fs';
import { Readable } from 'node:stream';
import { DiscordRoleModel } from '../models/discord.role.model';
import { LogService } from './log.service';

export class PrivateVoiceChannelService {
  private static readonly listPrivateVoiceChannelsFileName = 'list-salons-vocaux-prives';

  public static async getPrivateVoiceChannelsByGuild(
    guildId: Snowflake,
    limit: number,
    offset: number
  ): Promise<PrivateVoiceChannelModel[]> {
    return PrivateVoiceChannelModel.findAll({
      where: { privateVoiceChannelFeatureGuildId: guildId },
      include: [
        PrivateVoiceChannelModel.associations.discordUser,
        PrivateVoiceChannelModel.associations.discordUserGuests,
      ],
      limit,
      offset,
    });
  }

  public static async getPrivateVoiceChannelsByGuildAndUser(
    guildId: Snowflake,
    discordUserId: Snowflake,
    limit: number,
    offset: number
  ): Promise<{ rows: PrivateVoiceChannelModel[]; count: number }> {
    return PrivateVoiceChannelModel.findAndCountAll({
      where: { privateVoiceChannelFeatureGuildId: guildId, discordUserId },
      limit,
      offset,
    });
  }

  public static async deletePrivateVoiceChannelListFile(fileName: string): Promise<void> {
    await fsPromises.rm(fileName);
  }

  public static async createPrivateVoiceChannelListFile(guildId: Snowflake): Promise<string> {
    const countPrivateVoiceChannels = await this.countPrivateVoiceChannelsForGuild(guildId);
    const fileName = `${this.listPrivateVoiceChannelsFileName}-guild-${guildId}.txt`;
    const writeStream = fs.createWriteStream(fileName).on('error', (err) => {
      throw err;
    });
    const limit = 20;
    let rank = 1;
    let offset = 0;
    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannelModels = await this.getPrivateVoiceChannelsByGuild(guildId, limit, offset);
      privateVoiceChannelModels.forEach((pvc) => {
        writeStream.write(
          `${rank.toString()} - NOM : ${pvc.name} - CREE LE : ${pvc.createdAt.toUTCString()} - PROPRIETAIRE : ${
            pvc.discordUser.name
          } - PREMIUM : ${pvc.isPremium ? 'oui' : 'non'}\nINVITES : ${
            pvc.discordUserGuests.length > 0 ? pvc.discordUserGuests.map((u) => u.name).join(', ') : 'AUCUN'
          }\n\n`
        );
      });
      offset += limit;
      rank++;
    }
    writeStream.end();
    return fileName;
  }

  public static async createPrivateVoiceChannel(
    chatInputCommandInteraction: ChatInputCommandInteraction,
    name: string | null
  ): Promise<Snowflake> {
    const guild = chatInputCommandInteraction.guild;
    const guildId = chatInputCommandInteraction.guildId;
    const privateVoiceChannelFeatureModel =
      await PrivateVoiceChannelFeatureService.getPrivateVoiceChannelFeature(guildId);
    const guildMember = chatInputCommandInteraction.member as GuildMember;
    let discordUserModel = await DiscordUserModel.findByPk(guildMember.id);
    if (!discordUserModel) {
      discordUserModel = await DiscordUserModel.create({ id: guildMember.id, name: guildMember.displayName });
    }
    const countPrivateVoiceChannels = await discordUserModel.countPrivateVoiceChannels();
    const channelName = name
      ? name
      : `Salon-${(countPrivateVoiceChannels + 1).toString()}-de-${guildMember.displayName}`;
    const voiceChannel = await guild.channels.create<ChannelType.GuildVoice>({
      type: ChannelType.GuildVoice,
      parent: privateVoiceChannelFeatureModel.categoryId,
      name: channelName,
      permissionOverwrites: [
        {
          id: guildMember.id,
          allow: [
            PermissionFlagsBits.ViewChannel,
            PermissionFlagsBits.Connect,
            PermissionFlagsBits.ChangeNickname,
            PermissionFlagsBits.KickMembers,
            PermissionFlagsBits.Stream,
          ],
          deny: [PermissionFlagsBits.CreateInstantInvite],
        },
        {
          id: guild.roles.everyone.id,
          deny: [PermissionFlagsBits.Connect],
        },
      ],
    });

    try {
      const countVipRoles = await PrivateVoiceChannelFeatureService.countVipRoles(guildId);
      let offset = 0;
      const limit = 20;

      while (countVipRoles > offset) {
        const vipRoles = await PrivateVoiceChannelFeatureService.getVipRoles(guildId, limit, offset);
        const vipRolesStream = Readable.from(vipRoles);

        for await (const vipRole of vipRolesStream as unknown as DiscordRoleModel[]) {
          await voiceChannel.permissionOverwrites.create(vipRole.id, {
            ViewChannel: true,
            Connect: true,
            Stream: true,
            CreateInstantInvite: false,
          });
        }

        offset += limit;
      }

      const isPremium =
        privateVoiceChannelFeatureModel.premiumDiscordRoleId &&
        guildMember.roles.cache.has(privateVoiceChannelFeatureModel.premiumDiscordRoleId);
      await PrivateVoiceChannelModel.create({
        id: voiceChannel.id,
        name: channelName,
        isPremium,
        discordUserId: guildMember.id,
        privateVoiceChannelFeatureGuildId: guildId,
      });

      return voiceChannel.id;
    } catch (error) {
      await voiceChannel.delete();
      throw error;
    }
  }

  public static async destroyPrivateVoiceChannel(voiceChannel: VoiceChannel): Promise<boolean> {
    const affectedRows = await PrivateVoiceChannelModel.destroy({
      where: { id: voiceChannel.id, privateVoiceChannelFeatureGuildId: voiceChannel.guildId },
    });
    return affectedRows === 1;
  }

  public static async removePrivateVoiceChannel(channel: VoiceChannel): Promise<void> {
    const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(channel.id);
    return SequelizeClient.client.transaction(async (transaction) => {
      await privateVoiceChannelModel.destroy({ transaction });
      await channel.delete();
    });
  }

  public static async renamePrivateVoiceChannel(channel: VoiceChannel, nom: string): Promise<void> {
    return SequelizeClient.client.transaction(async (transaction) => {
      await PrivateVoiceChannelModel.update({ name: nom }, { where: { id: channel.id }, transaction });
      await channel.setName(nom);
    });
  }

  public static async inviteUserToPrivateVoiceChannel(channel: VoiceChannel, guildMember: GuildMember): Promise<void> {
    return SequelizeClient.client.transaction(async (transaction) => {
      const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(channel.id, { transaction });
      const [discordUserModel] = await DiscordUserModel.findOrCreate({
        where: { id: guildMember.id },
        defaults: { name: guildMember.displayName },
        transaction,
      });
      await privateVoiceChannelModel.addDiscordUserGuest(discordUserModel, { transaction });
      await channel.permissionOverwrites.create(guildMember.id, {
        ViewChannel: true,
        Connect: true,
        Stream: true,
        CreateInstantInvite: false,
      });
    });
  }

  public static async removeGuestFromPrivateVoiceChannel(channel: VoiceChannel, guest: GuildMember): Promise<void> {
    await SequelizeClient.client.transaction(async (transaction) => {
      const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(channel.id);
      await privateVoiceChannelModel.removeDiscordUserGuest(guest.id, { transaction });
      if (guest.voice.channel?.id === privateVoiceChannelModel.id) {
        await guest.voice.disconnect();
      }
      await channel.permissionOverwrites.delete(guest.id);
    });
    await DiscordUserService.removeDiscordUserIfHasNoAssociations(guest.id);
  }

  public static async getPrivateVoiceChannelGuests(channel: VoiceChannel, limit: number, offset: number) {
    const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(channel.id);
    return privateVoiceChannelModel.getDiscordUserGuests({ limit, offset });
  }

  public static async updatePrivateVoiceChannel(channel: VoiceChannel): Promise<boolean> {
    const [affectedCount] = await PrivateVoiceChannelModel.update(
      { name: channel.name },
      { where: { id: channel.id, privateVoiceChannelFeatureGuildId: channel.guildId } }
    );
    return affectedCount > 0;
  }

  public static async countPrivateVoiceChannelsForGuild(guildId: Snowflake): Promise<number> {
    return PrivateVoiceChannelModel.count({ where: { privateVoiceChannelFeatureGuildId: guildId } });
  }

  public static async countPrivateVoiceChannelsForDiscordUser(
    guildId: Snowflake,
    discordUserId: Snowflake
  ): Promise<number> {
    return PrivateVoiceChannelModel.count({ where: { privateVoiceChannelFeatureGuildId: guildId, discordUserId } });
  }

  public static async isOwner(discordUserId: Snowflake, privateVoiceChannelId: Snowflake): Promise<boolean> {
    const count = await PrivateVoiceChannelModel.count({ where: { id: privateVoiceChannelId, discordUserId } });
    return count === 1;
  }

  public static async hasDiscordUserGuest(
    discordUserId: Snowflake,
    privateVoiceChannelId: Snowflake
  ): Promise<boolean> {
    const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(privateVoiceChannelId);
    return privateVoiceChannelModel.hasDiscordUserGuest(discordUserId);
  }

  public static async countDiscordUserGuests(privateVoiceChannelId: Snowflake): Promise<number> {
    const privateVoiceChannelModel = await PrivateVoiceChannelModel.findByPk(privateVoiceChannelId);
    return privateVoiceChannelModel.countDiscordUserGuests();
  }

  public static async addVipRoleToExistingPrivateVoiceChannels(guildId: Snowflake, role: Role): Promise<void> {
    const countPrivateVoiceChannels = await PrivateVoiceChannelModel.count({
      where: { privateVoiceChannelFeatureGuildId: guildId },
    });
    let offset = 0;
    const limit = 20;

    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannels = await this.getPrivateVoiceChannelsByGuild(guildId, limit, offset);
      const privateVoiceChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of privateVoiceChannelStream as unknown as PrivateVoiceChannelModel[]) {
        const voiceChannel = (await role.guild.channels.fetch(pvc.id)) as VoiceChannel;
        await voiceChannel.permissionOverwrites.create(role, {
          ViewChannel: true,
          Connect: true,
          Stream: true,
          CreateInstantInvite: false,
        });
        await LogService.info(`Vip role supprime pour le private voice channel ${pvc.id} dans la guild ${guildId}`);
      }

      offset += limit;
    }
  }

  public static async removeVipRoleToExistingPrivateVoiceChannels(guildId: Snowflake, role: Role): Promise<void> {
    const countPrivateVoiceChannels = await PrivateVoiceChannelModel.count({
      where: { privateVoiceChannelFeatureGuildId: guildId },
    });
    let offset = 0;
    const limit = 20;

    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannels = await this.getPrivateVoiceChannelsByGuild(guildId, limit, offset);
      const privateVoiceChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of privateVoiceChannelStream as unknown as PrivateVoiceChannelModel[]) {
        const voiceChannel = (await role.guild.channels.fetch(pvc.id)) as VoiceChannel;
        await voiceChannel.permissionOverwrites.delete(role);
        await LogService.info(`Vip role supprime pour le private voice channel ${pvc.id} dans la guild ${guildId}`);
      }

      offset += limit;
    }
  }

  public static async addPremiumFlagToPrivateVoiceChannels(guildId: Snowflake, role: Role): Promise<void> {
    const countPrivateVoiceChannels = await PrivateVoiceChannelModel.count({
      where: { privateVoiceChannelFeatureGuildId: guildId, isPremium: false },
    });
    let offset = 0;
    const limit = 20;

    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannels = await PrivateVoiceChannelModel.findAll({
        where: { privateVoiceChannelFeatureGuildId: guildId, isPremium: false },
        limit,
        offset,
      });
      const privateVoiceChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of privateVoiceChannelStream as unknown as PrivateVoiceChannelModel[]) {
        const guildMember = await role.guild.members.fetch(pvc.discordUserId);
        if (!guildMember.roles.cache.has(role.id)) {
          continue;
        }
        pvc.isPremium = true;
        await pvc.save();
        await LogService.info(`Flag premium ajoute pour le private voice channel ${pvc.id} dans la guild ${guildId}`);
      }

      offset += limit;
    }
  }

  public static async removePremiumFlagToPrivateVoiceChannels(guildId: Snowflake): Promise<void> {
    const countPrivateVoiceChannels = await PrivateVoiceChannelModel.count({
      where: { privateVoiceChannelFeatureGuildId: guildId, isPremium: true },
    });
    let offset = 0;
    const limit = 20;

    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannels = await PrivateVoiceChannelModel.findAll({
        where: { privateVoiceChannelFeatureGuildId: guildId, isPremium: true },
        limit,
        offset,
      });
      const privateVoiceChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of privateVoiceChannelStream as unknown as PrivateVoiceChannelModel[]) {
        pvc.isPremium = false;
        await pvc.save();
        await LogService.info(`Flag premium supprime pour le private voice channel ${pvc.id} dans la guild ${guildId}`);
      }

      offset += limit;
    }
  }

  public static async switchPremiumFlagToPrivateVoiceChannelsForGuildMember(guildMember: GuildMember): Promise<void> {
    const guildId = guildMember.guild.id;
    const countPrivateVoiceChannels = await PrivateVoiceChannelModel.count({
      where: { privateVoiceChannelFeatureGuildId: guildId, discordUserId: guildMember.id },
    });
    let offset = 0;
    const limit = 20;

    while (countPrivateVoiceChannels > offset) {
      const privateVoiceChannels = await PrivateVoiceChannelModel.findAll({
        where: { privateVoiceChannelFeatureGuildId: guildId, discordUserId: guildMember.id },
        limit,
        offset,
      });
      const privateVoiceChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of privateVoiceChannelStream as unknown as PrivateVoiceChannelModel[]) {
        pvc.isPremium = !pvc.isPremium;
        await pvc.save();
        await LogService.info(
          `Flag premium ${pvc.isPremium ? 'ajoute' : 'supprime'} pour le private voice channel ${
            pvc.id
          } dans la guild ${guildId}`
        );
      }

      offset += limit;
    }
  }
}
