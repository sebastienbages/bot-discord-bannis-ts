import { SlashCommandInterface } from '../interfaces/slash.command.interface';
import { SlashCommandBuilder } from '@discordjs/builders';
import { Config } from '../config/config';
import { SurveyCommand } from '../interactions/commands/utils.admin/survey.command';
import { TicketCommand } from '../interactions/commands/utils.admin/ticket.command';
import { VoteCommand } from '../interactions/commands/utils.admin/vote.command';
import { RoleCommand } from '../interactions/commands/utils.role/role.command';
import { ClearCommand } from '../interactions/commands/utils.message/clear.command';
import { SayCommand } from '../interactions/commands/utils.message/say.command';
import { SayPrivCommand } from '../interactions/commands/utils.message/say.priv.command';
import { TopServerCommand } from '../interactions/commands/utils.admin/top.server.command';
import { Collection, RESTPostAPIApplicationCommandsJSONBody, Routes, REST } from 'discord.js';
import { LogCommand } from '../interactions/commands/utils.admin/log.command';
import { LogService } from './log.service';
import { Bot } from '../bot';
import { VocalAdminCommand } from '../interactions/commands/private.voice.channel.feature/vocal.admin.command';
import { VocalCommand } from '../interactions/commands/private.voice.channel.feature/vocal.command';

export class SlashCommandService {
  /**
   * Liste des commandes à enregistrer
   *
   * @private
   */
  private static readonly commands = [
    SurveyCommand,
    TicketCommand,
    VoteCommand,
    RoleCommand,
    ClearCommand,
    SayCommand,
    SayPrivCommand,
    LogCommand,
    ...(Config.tokenTopServer ? [TopServerCommand] : []),
    VocalAdminCommand,
    VocalCommand,
  ];

  private static commandsInstances = new Collection<string, SlashCommandInterface>();

  public static async initialize() {
    this.commands.forEach((commandClass) => {
      const instance = new commandClass() as SlashCommandInterface;
      this.commandsInstances.set(instance.command.name, instance);
    });
    await LogService.info('Slash command service OK');
  }

  /**
   * Enregistre les slashs commandes dans l'application
   */
  public static async registerSlashCommands(): Promise<void> {
    const rest = new REST({ version: '10' }).setToken(Config.token);
    await rest.put(Routes.applicationGuildCommands(Bot.applicationId, Bot.guildId), {
      body: this.getSlashCommandsToJson(),
    });
    await LogService.info('Commandes enregistrees');
  }

  /**
   * Retourne la commande
   *
   * @param name
   */
  public static getInstance(name: string): SlashCommandInterface {
    return this.commandsInstances.get(name);
  }

  /**
   * Retourne le build des slashs commandes au format JSON
   *
   * @private
   */
  private static getSlashCommandsToJson(): RESTPostAPIApplicationCommandsJSONBody[] {
    const slashCommandBuilders = this.getSlashCommands();
    return slashCommandBuilders.map((cmd) => cmd.toJSON());
  }

  /**
   * Retourne les slashs commandes de l'application
   *
   * @private
   */
  private static getSlashCommands(): SlashCommandBuilder[] {
    const commands: SlashCommandBuilder[] = [];
    this.commandsInstances.forEach((slashCommand) => {
      commands.push(slashCommand.command);
    });
    return commands;
  }
}
