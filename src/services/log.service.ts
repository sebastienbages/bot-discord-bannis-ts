import * as fsPromises from 'node:fs/promises';
import date from 'date-and-time';
import { bold, Client } from 'discord.js';
import { Config } from '../config/config';
import { AppError } from '../error/app.error';
import path from 'node:path';

export class LogService {
  private static readonly logPath = './';
  private static readonly fileName = `bannis-bot-discord-logs-${this.now()}.txt`;

  public static async info(message: string): Promise<void> {
    message = `${this.now()} - ${message}`;
    console.log(message);
    await this.writeLog(message);
  }

  public static async error(error: Error): Promise<void> {
    const message = `${this.now()} - ${error.stack}`;
    console.error(message);
    await this.writeLog(message);
  }

  public static async toDeveloper(client: Client, message: string): Promise<void> {
    try {
      const dev = client.users.cache.get(Config.devId) || (await client.users.fetch(Config.devId));
      if (dev) {
        await dev.send({ content: message });
      }
    } catch (error) {
      await this.error(error as Error);
    }
  }

  public static async handlerError(error: Error, client: Client): Promise<void> {
    if (error.name === 'DiscordAPIError') {
      await this.toDeveloper(
        client,
        `${bold("Une erreur api discord s'est produite :")}\n${error.message}\n${error.stack}`
      );
    } else {
      await this.toDeveloper(
        client,
        `${bold("Une erreur inattendue s'est produite :")}\n${error.message}\n${error.stack}`
      );
    }
    return this.error(error);
  }

  public static async handlerAppError(error: AppError, client: Client): Promise<void> {
    await this.toDeveloper(
      client,
      `${bold("Une erreur d'application non gérée s'est produite :")}\n${error.name}\n${error.message}`
    );
    return this.error(error);
  }

  public static getLogFileName(): string {
    return path.join(this.logPath, this.fileName);
  }

  private static now(): string {
    return date.format(new Date(), 'DD.MM.YYYY-HH.mm.ss');
  }

  private static async writeLog(message: string): Promise<void> {
    try {
      if (Config.nodeEnv !== Config.nodeEnvValues.production) {
        return;
      }
      await fsPromises.appendFile(path.join(this.logPath, this.fileName), `${message}\n`);
    } catch (error) {
      console.error(error);
    }
  }
}
