import { PrivateVoiceChannelFeatureModel } from '../models/private.voice.channel.feature.model';
import { LogService } from './log.service';
import { CategoryChannel, Collection, Guild, Role, Snowflake, VoiceChannel } from 'discord.js';
import { Includeable, Op, Transaction } from 'sequelize';
import { SequelizeClient } from '../database/client/sequelize.client';
import { DiscordRoleModel } from '../models/discord.role.model';
import { PrivateVoiceChannelModel } from '../models/private.voice.channel.model';
import { Readable } from 'node:stream';
import { AppError } from '../error/app.error';

export class PrivateVoiceChannelFeatureService {
  private static timerPrivateVoiceChannelErasers = new Collection<Snowflake, NodeJS.Timeout>();

  public static async initialize(guild: Guild): Promise<void> {
    const guildId = guild.id;
    let privateVoiceChannelFeatureModel = await PrivateVoiceChannelFeatureModel.findByPk(guildId);
    if (!privateVoiceChannelFeatureModel) {
      privateVoiceChannelFeatureModel = await this.createFeature(guildId);
      await LogService.info(`Private voice channel feature init in database for guild ${guild.id}`);
    }
    if (privateVoiceChannelFeatureModel.isActive) {
      await this.createAutoCleaner(guild, privateVoiceChannelFeatureModel.timeLimitInHours);
    }
  }

  public static async createFeature(guildId: Snowflake): Promise<PrivateVoiceChannelFeatureModel> {
    return PrivateVoiceChannelFeatureModel.create({ guildId });
  }

  public static async createAutoCleaner(guild: Guild, timeLimit: number): Promise<void> {
    this.timerPrivateVoiceChannelErasers.set(
      guild.id,
      setInterval(() => void this.removeExpiredPrivateVoiceChannels(guild), timeLimit * 60 * 60 * 1000)
    );
    await LogService.info(`Effacement auto private voice channel demarre pour la guild ${guild.id}`);
  }

  public static async deleteAutoCleaner(guildId: Snowflake): Promise<void> {
    const timer = this.timerPrivateVoiceChannelErasers.get(guildId);
    clearInterval(timer);
    await LogService.info(`Effacement auto voice channel cleaner supprime pour la guild ${guildId}`);
  }

  public static async deleteFeature(guild: Guild): Promise<void> {
    const guildId = guild.id;
    this.timerPrivateVoiceChannelErasers.delete(guildId);
    await PrivateVoiceChannelFeatureModel.destroy({ where: { guildId } });
  }

  public static async featureIsActive(guildId: Snowflake): Promise<boolean> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    return privateVoiceChannelFeatureModel.isActive;
  }

  public static async enableDisableFeature(guild: Guild): Promise<boolean> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guild.id);

    if (!privateVoiceChannelFeatureModel.categoryId && !privateVoiceChannelFeatureModel.isActive) {
      throw new AppError('enableDisableFeature', 'la categorie est absente');
    }

    return SequelizeClient.client.transaction(async (transaction) => {
      privateVoiceChannelFeatureModel.isActive = !privateVoiceChannelFeatureModel.isActive;
      await privateVoiceChannelFeatureModel.save({ transaction });

      if (privateVoiceChannelFeatureModel.isActive) {
        await this.createAutoCleaner(guild, privateVoiceChannelFeatureModel.timeLimitInHours);
      } else {
        await this.deleteAutoCleaner(guild.id);
      }

      return privateVoiceChannelFeatureModel.isActive;
    });
  }

  public static async saveCategory(category: CategoryChannel): Promise<void> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(category.guildId);

    if (privateVoiceChannelFeatureModel.categoryId === category.id) {
      throw new AppError('saveCategory', `la categorie ${category.name} est deja selectionnee`);
    }

    privateVoiceChannelFeatureModel.categoryId = category.id;
    await privateVoiceChannelFeatureModel.save();
  }

  public static async setMaxVoiceChannelByUser(guildId: Snowflake, commandValue: number): Promise<void> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);

    if (privateVoiceChannelFeatureModel.maxVoiceChannelByUser === commandValue) {
      throw new AppError('setMaxVoiceChannelByUser', `la limite de ${commandValue.toString()} est identique`);
    }

    privateVoiceChannelFeatureModel.maxVoiceChannelByUser = commandValue;
    await privateVoiceChannelFeatureModel.save();
  }

  public static async setTimeLimit(guild: Guild, hours: number): Promise<void> {
    const guildId = guild.id;
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);

    if (privateVoiceChannelFeatureModel.timeLimitInHours === hours) {
      throw new AppError('setTimeLimit', `le delai ${hours.toString()} est identique`);
    }

    return SequelizeClient.client.transaction(async (transaction) => {
      privateVoiceChannelFeatureModel.timeLimitInHours = hours;
      await privateVoiceChannelFeatureModel.save({ transaction });
      await this.deleteAutoCleaner(guildId);
      await this.createAutoCleaner(guild, privateVoiceChannelFeatureModel.timeLimitInHours);
    });
  }

  public static async setMaxVoiceChannelInGuild(guildId: Snowflake, commandValue: number) {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);

    if (privateVoiceChannelFeatureModel.maxVoiceChannelInGuild === commandValue) {
      throw new AppError('setMaxVoiceChannelInGuild', `la limite de ${commandValue.toString()} est identique`);
    }

    privateVoiceChannelFeatureModel.maxVoiceChannelInGuild = commandValue;
    await privateVoiceChannelFeatureModel.save();
  }

  public static async getPrivateVoiceChannelFeature(
    guildId: Snowflake,
    ...associations: Includeable[]
  ): Promise<PrivateVoiceChannelFeatureModel> {
    return PrivateVoiceChannelFeatureModel.findByPk(guildId, { include: associations });
  }

  public static async getMaxPrivateVoiceChannelByUser(guildId: Snowflake): Promise<number> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    return privateVoiceChannelFeatureModel.maxVoiceChannelByUser;
  }

  public static async isPremiumRole(role: Role): Promise<boolean> {
    const result = await PrivateVoiceChannelFeatureModel.count({
      where: { guildId: role.guild.id, premiumDiscordRoleId: role.id },
    });
    return result > 0;
  }

  public static async countVipRoles(guildId: Snowflake): Promise<number> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    return privateVoiceChannelFeatureModel.countVipRoles();
  }

  public static async hasVipRole(role: Role): Promise<boolean> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(role.guild.id);
    return privateVoiceChannelFeatureModel.hasVipRole(role.id);
  }

  public static async addVipRole(role: Role): Promise<void> {
    return SequelizeClient.client.transaction(async (transaction) => {
      const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(role.guild.id);
      const [discordRole] = await DiscordRoleModel.findOrCreate({
        where: { id: role.id },
        transaction,
        lock: true,
      });
      await privateVoiceChannelFeatureModel.addVipRole(discordRole, { transaction });
    });
  }

  public static async removeVipRole(role: Role): Promise<void> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(role.guild.id);
    const discordRole = await DiscordRoleModel.findByPk(role.id);
    await privateVoiceChannelFeatureModel.removeVipRole(discordRole);
    await this.removeDiscordRoleIfHasNoAssociations(role.id);
  }

  public static async addPremiumRole(role: Role): Promise<void> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(role.guild.id);
    privateVoiceChannelFeatureModel.premiumDiscordRoleId = role.id;
    await privateVoiceChannelFeatureModel.save();
  }

  public static async removePremiumRole(guildId: Snowflake): Promise<void> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    privateVoiceChannelFeatureModel.premiumDiscordRoleId = null;
    await privateVoiceChannelFeatureModel.save();
  }

  public static async getVipRoles(guildId: Snowflake, limit: number, offset: number): Promise<DiscordRoleModel[]> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    return privateVoiceChannelFeatureModel.getVipRoles({ limit, offset });
  }

  public static async getExpiredPrivateVoiceChannels(
    guildId: Snowflake,
    limit: number,
    offset: number
  ): Promise<{
    rows: Array<
      Omit<PrivateVoiceChannelModel, 'name' | 'discordUserId' | 'privateVoiceChannelFeatureGuildId' | 'createdAt'>
    >;
    count: number;
  }> {
    const privateVoiceChannelFeatureModel = await this.getPrivateVoiceChannelFeature(guildId);
    return PrivateVoiceChannelModel.findAndCountAll({
      attributes: ['id'],
      where: {
        privateVoiceChannelFeatureGuildId: guildId,
        isPremium: false,
        createdAt: {
          [Op.lt]: new Date(Date.now() - privateVoiceChannelFeatureModel.timeLimitInHours * 60 * 60 * 1000),
        },
      },
      limit,
      offset,
    });
  }

  public static async removeExpiredPrivateVoiceChannels(guild: Guild): Promise<void> {
    await LogService.info(`Demarrage de l'effacement automatique des private voice channels pour la guild ${guild.id}`);
    let remainingPrivateVoiceChannels = 1;
    while (remainingPrivateVoiceChannels !== 0) {
      const { rows: privateVoiceChannels, count } = await this.getExpiredPrivateVoiceChannels(guild.id, 20, 0);
      remainingPrivateVoiceChannels = count;

      if (remainingPrivateVoiceChannels === 0) {
        break;
      }

      const deleteChannelStream = Readable.from(privateVoiceChannels);

      for await (const pvc of deleteChannelStream) {
        const privateVoiceChannel = pvc as PrivateVoiceChannelModel;
        const voiceChannel = (await guild.channels.fetch(privateVoiceChannel.id)) as VoiceChannel;
        if (voiceChannel.members.size === 0) {
          void guild.channels.delete(privateVoiceChannel.id);
          await privateVoiceChannel.destroy();
          await LogService.info(
            `Effacement auto du private voice channels ${voiceChannel.id} dans la guild ${guild.id}`
          );
        }
      }
    }
    await LogService.info(`Effacement automatique des private voice channels pour la guild ${guild.id} terminé`);
  }

  private static async removeDiscordRoleIfHasNoAssociations(
    discordRoleId: Snowflake,
    transaction?: Transaction
  ): Promise<void> {
    const discordRoleModel = await DiscordRoleModel.findByPk(discordRoleId);
    if (discordRoleModel && !(await discordRoleModel.hasAssociations())) {
      await discordRoleModel.destroy({ transaction });
    }
  }
}
