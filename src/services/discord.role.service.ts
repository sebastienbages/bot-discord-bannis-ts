import { AttachmentBuilder, ButtonInteraction, GuildMember, Snowflake } from 'discord.js';
import { Config } from '../config/config';
import util from 'util';
import { DiscordRoleModel } from '../models/discord.role.model';

export class DiscordRoleService {
  public static userHasRole(roleId: string, user: GuildMember): boolean {
    return user.roles.cache.has(roleId);
  }

  public static async setRole(roleId: string, guildMember: GuildMember): Promise<GuildMember> {
    return guildMember.roles.add(roleId);
  }

  public static async removeRole(roleId: string, guildMember: GuildMember): Promise<GuildMember> {
    return guildMember.roles.remove(roleId);
  }

  public static async assignStartRole(buttonInteraction: ButtonInteraction): Promise<void> {
    const guildMember = buttonInteraction.member as GuildMember;
    const wait = util.promisify(setTimeout);
    const teleportationImage = new AttachmentBuilder(Config.imageDir + '/teleportation.gif');
    await buttonInteraction.editReply({
      content: "Ok c'est parti ! Accroche ta ceinture ça va secouer :rocket:",
      files: [teleportationImage],
    });
    await wait(8000);
    await this.setRole(Config.roleStartId, guildMember);
    await this.removeRole(Config.roleFrontiereId, guildMember);
  }

  public static async removeDiscordRole(roleId: Snowflake): Promise<boolean> {
    const affectedRows = await DiscordRoleModel.destroy({ where: { id: roleId } });
    return affectedRows === 1;
  }
}
