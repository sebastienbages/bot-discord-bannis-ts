import { GuildMember, Snowflake } from 'discord.js';
import { Transaction } from 'sequelize';
import { DiscordUserModel } from '../models/discord.user.model';

export class DiscordUserService {
  public static async removeDiscordUserIfHasNoAssociations(
    discordUserId: Snowflake,
    transaction?: Transaction
  ): Promise<void> {
    const discordUserModel = await DiscordUserModel.findByPk(discordUserId);
    if (discordUserModel && !(await discordUserModel.hasAssociations())) {
      await discordUserModel.destroy({ transaction });
    }
  }

  public static async removeDiscordUser(discordUserId: Snowflake): Promise<boolean> {
    const affectedRows = await DiscordUserModel.destroy({ where: { id: discordUserId } });
    return affectedRows > 0;
  }

  public static async updateDiscordUser(guildMember: GuildMember): Promise<void> {
    await DiscordUserModel.update({ name: guildMember.displayName }, { where: { id: guildMember.id } });
  }
}
