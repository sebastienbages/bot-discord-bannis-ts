import { ButtonInterface } from '../interfaces/button.interface';
import { ButtonBuilder, Collection } from 'discord.js';
import { CloseTicketButton } from '../interactions/buttons/close.ticket.button';
import { CreateTicketButton } from '../interactions/buttons/create.ticket.button';
import { DeleteTicketButton } from '../interactions/buttons/delete.ticket.button';
import { OpenTicketButton } from '../interactions/buttons/open.ticket.button';
import { ValidationRulesButton } from '../interactions/buttons/validation.rules.button';
import { LogService } from './log.service';
import { OpenTicketServiceButton } from '../interactions/buttons/open.ticket.service.button';
import { CloseTicketServiceButton } from '../interactions/buttons/close.ticket.service.button';

export const enum ButtonList {
  CloseTicket = 'closeTicket',
  CreateTicket = 'createTicket',
  DeleteTicket = 'deleteTicket',
  OpenTicket = 'reOpenTicket',
  ValidationRules = 'validation_rules',
  OpenTicketService = 'openTicketService',
  CloseTicketService = 'closeTicketService',
}

export class ButtonService {
  private static readonly buttons = [
    CloseTicketButton,
    CreateTicketButton,
    DeleteTicketButton,
    OpenTicketButton,
    ValidationRulesButton,
    OpenTicketServiceButton,
    CloseTicketServiceButton,
  ];

  private static readonly buttonsInstanceCollection = new Collection<string, ButtonInterface>();

  public static async initialize() {
    this.buttons.forEach((buttonClass) => {
      const instance = new buttonClass() as ButtonInterface;
      this.buttonsInstanceCollection.set(instance.customId, instance);
    });
    await LogService.info('Button service OK');
  }

  /**
   * Retourne l'instance du bouton selon son Id
   *
   * @param customId
   */
  public static getInstance(customId: ButtonList | string): ButtonInterface {
    return this.buttonsInstanceCollection.get(customId);
  }

  /**
   * Retourne l'instance d'un bouton selon son Id
   *
   * @param customId
   */
  public static getButton(customId: ButtonList | string): ButtonBuilder {
    return this.buttonsInstanceCollection.get(customId).getButtonInstance();
  }
}
