import { DMChannel, Guild, GuildChannel, GuildMember, Interaction, Message, Role } from 'discord.js';
import * as dotenv from 'dotenv';
import { Config } from './config/config';
import { Bot } from './bot';
import { GuildMemberAddEvent } from './events/guild.member.add.event';
import { GuildMemberRemoveEvent } from './events/guild.member.remove.event';
import { InteractionCreateEvent } from './events/interaction.create.event';
import { MessageCreateEvent } from './events/message.create.event';
import { RoleDeleteEvent } from './events/role.delete.event';
import { ChannelDeleteEvent } from './events/channel.delete.event';
import { ChannelUpdateEvent } from './events/channel.update.event';
import { GuildMemberUpdateEvent } from './events/guild.member.update.event';
import { GuildCreateEvent } from './events/guild.create.event';
import { GuildDeleteEvent } from './events/guild.delete.event';

dotenv.config();

const bot: Bot = new Bot(Config.token);
void bot.start();

bot.on('messageCreate', (message: Message) => MessageCreateEvent.run(message));
bot.on('guildMemberAdd', (member: GuildMember) => GuildMemberAddEvent.run(member));
bot.on('guildMemberRemove', (member: GuildMember) => GuildMemberRemoveEvent.run(member));
bot.on('guildMemberUpdate', (oldMember: GuildMember, newMember: GuildMember) =>
  GuildMemberUpdateEvent.run(oldMember, newMember)
);
bot.on('interactionCreate', (interaction: Interaction) => InteractionCreateEvent.run(interaction));
bot.on('roleDelete', (role: Role) => RoleDeleteEvent.run(role));
bot.on('channelDelete', (channel: DMChannel | GuildChannel) => ChannelDeleteEvent.run(channel));
bot.on('channelUpdate', (oldChannel: DMChannel | GuildChannel, newChannel: DMChannel | GuildChannel) =>
  ChannelUpdateEvent.run(oldChannel, newChannel)
);
bot.on('guildCreate', (guild: Guild) => GuildCreateEvent.run(guild));
bot.on('guildDelete', (guild: Guild) => GuildDeleteEvent.run(guild));
