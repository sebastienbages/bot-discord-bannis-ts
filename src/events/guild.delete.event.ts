import { Guild } from 'discord.js';
import { LogService } from '../services/log.service';
import { PrivateVoiceChannelFeatureService } from '../services/private.voice.channel.feature.service';

export class GuildDeleteEvent {
  public static async run(guild: Guild): Promise<void> {
    try {
      await LogService.info(`Guild ${guild.id} supprimee`);
      await PrivateVoiceChannelFeatureService.deleteFeature(guild);
    } catch (error) {
      await LogService.handlerError(error as Error, guild.client);
    }
  }
}
