import { GuildMember } from 'discord.js';
import { DiscordUserService } from '../services/discord.user.service';
import { PrivateVoiceChannelFeatureService } from '../services/private.voice.channel.feature.service';
import { PrivateVoiceChannelService } from '../services/private.voice.channel.service';
import { LogService } from '../services/log.service';

export class GuildMemberUpdateEvent {
  public static async run(oldMember: GuildMember, newMember: GuildMember): Promise<void> {
    if (oldMember.displayName !== newMember.displayName) {
      await DiscordUserService.updateDiscordUser(newMember);
    }

    const privateVoiceChannelFeature = await PrivateVoiceChannelFeatureService.getPrivateVoiceChannelFeature(
      newMember.guild.id
    );
    const premiumRoleId = privateVoiceChannelFeature.premiumDiscordRoleId;

    const oldMemberIsPremium = oldMember.roles.cache.has(premiumRoleId);
    const newMemberIsPremium = newMember.roles.cache.has(premiumRoleId);
    const premiumRoleIsRemovedForNewMember = oldMemberIsPremium && !newMemberIsPremium;
    const premiumRoleIsAddedForNewMember = !oldMemberIsPremium && newMemberIsPremium;

    if (premiumRoleId && (premiumRoleIsRemovedForNewMember || premiumRoleIsAddedForNewMember)) {
      await PrivateVoiceChannelService.switchPremiumFlagToPrivateVoiceChannelsForGuildMember(newMember);
      await LogService.info(
        `Role premium ${premiumRoleId} ${premiumRoleIsAddedForNewMember ? 'ajoute' : 'supprime'} pour le membre ${
          newMember.id
        } dans la guild ${newMember.guild.id}`
      );
    }
  }
}
