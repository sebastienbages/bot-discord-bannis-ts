import { LogService } from '../services/log.service';
import {
  ButtonInteraction,
  ChatInputCommandInteraction,
  GuildMember,
  Interaction,
  ModalSubmitInteraction,
  StringSelectMenuInteraction,
  Snowflake,
} from 'discord.js';
import { SlashCommandService } from '../services/slash.command.service';
import { SlashCommandInterface } from '../interfaces/slash.command.interface';
import { ButtonService } from '../services/button.service';
import { ButtonInterface } from '../interfaces/button.interface';
import { InteractionError } from '../error/interaction.error';
import { ModalInterface } from '../interfaces/modal.interface';
import { ModalService } from '../services/modal.service';
import { AppError } from '../error/app.error';

export class InteractionCreateEvent {
  private static lockInteraction: Snowflake[] = [];

  public static async run(interaction: Interaction): Promise<void> {
    const interactionObject = interaction as
      | ChatInputCommandInteraction
      | StringSelectMenuInteraction
      | ButtonInteraction
      | ModalSubmitInteraction;
    const guildMember = interaction.member as GuildMember;

    try {
      if (this.userIsLocked(guildMember.id)) {
        await interactionObject.reply({
          content: 'Flooder ne sert à rien ! :triumph:',
          ephemeral: true,
          fetchReply: false,
        });
        return LogService.info(`Le membre ${guildMember.displayName} ${guildMember.id} flood les interactions`);
      }

      this.lockUser(guildMember.id);

      if (interaction.isChatInputCommand()) {
        await LogService.info(
          `${guildMember.displayName} ${guildMember.id} a lance la commande "${
            interaction.commandName
          }" : [ ${interaction.options.data
            .map(
              (cio) =>
                `{ name: ${cio.name}${cio.value ? `, value: ${cio.value}` : ''}${
                  cio.options?.length > 0
                    ? `, options: [ ${cio.options
                        .map(
                          (subCommand) =>
                            `{ name: ${subCommand.name}${subCommand.value ? `, value: ${subCommand.value}` : ''} }`
                        )
                        .join(', ')} ]`
                    : ''
                } }`
            )
            .join(', ')} ]"`
        );
        const slashCommand: SlashCommandInterface = SlashCommandService.getInstance(interaction.commandName);
        return await slashCommand.execute(interaction);
      }

      if (interaction.isButton()) {
        await LogService.info(
          `${guildMember.displayName} ${guildMember.id} a appuye sur le bouton "${interaction.customId}"`
        );
        const instanceButton: ButtonInterface = ButtonService.getInstance(interaction.customId);
        return await instanceButton.execute(interaction);
      }

      if (interaction.isModalSubmit()) {
        await LogService.info(
          `${guildMember.displayName} ${guildMember.id} a soumis la modal "${interaction.customId}"`
        );
        const instanceModal: ModalInterface = ModalService.getInstance(interaction.customId);
        return await instanceModal.execute(interaction);
      }
    } catch (error) {
      if (!interactionObject.deferred) {
        await interactionObject.deferReply({ ephemeral: true, fetchReply: false });
      }

      if (error instanceof InteractionError) {
        const interactionError = error;
        await interactionObject.editReply({ content: interactionError.discordMessage });
        return LogService.info(`Erreur de "${interactionError.name}" : ${interactionError.message}`);
      }

      if (error instanceof AppError) {
        return LogService.handlerAppError(error, interaction.client);
      }

      await interactionObject.editReply({
        content:
          "Oups ! Une erreur s'est produite :thermometer_face: \nSi le problème persiste, merci de contacter un admin.",
      });
      return LogService.handlerError(error as Error, interaction.client);
    } finally {
      this.unlockUser(guildMember.id);
    }
  }

  /**
   * Vérifie si un utilisateur est verrouillé
   *
   * @param userSnowflake
   */
  private static userIsLocked(userSnowflake: Snowflake): boolean {
    return this.lockInteraction.includes(userSnowflake);
  }

  /**
   * Verrouille l'utilisateur
   *
   * @param userSnowflake
   * @private
   */
  private static lockUser(userSnowflake: Snowflake): void {
    this.lockInteraction.push(userSnowflake);
  }

  /**
   * Déverrouille l'utilisateur
   *
   * @param userSnowflake
   * @private
   */
  private static unlockUser(userSnowflake: Snowflake): void {
    if (this.userIsLocked(userSnowflake)) {
      const userIndex = this.lockInteraction.indexOf(userSnowflake);
      this.lockInteraction.splice(userIndex, 1);
    }
  }
}
