import { GuildMember, TextChannel, EmbedBuilder } from 'discord.js';
import { Config } from '../config/config';
import { LogService } from '../services/log.service';
import { Bot } from '../bot';
import { DiscordUserService } from '../services/discord.user.service';
import { PrivateVoiceChannelService } from '../services/private.voice.channel.service';
import { Readable } from 'node:stream';
import { PrivateVoiceChannelModel } from '../models/private.voice.channel.model';

export class GuildMemberRemoveEvent {
  public static async run(member: GuildMember): Promise<void> {
    try {
      await LogService.info(`Depart d'un membre : "${member.displayName}"`);
      const guildId = member.guild.id;
      const guildMemberId = member.id;
      const channelManager = member.guild.channels;
      let remainingPrivateVoiceChannels = 1;

      while (remainingPrivateVoiceChannels !== 0) {
        const { rows: privateVoiceChannels, count } =
          await PrivateVoiceChannelService.getPrivateVoiceChannelsByGuildAndUser(guildId, guildMemberId, 20, 0);
        remainingPrivateVoiceChannels = count;

        if (remainingPrivateVoiceChannels === 0) {
          break;
        }

        const deleteChannelStream = Readable.from(privateVoiceChannels);

        for await (const pvc of deleteChannelStream) {
          const privateVocalChannel = pvc as PrivateVoiceChannelModel;
          const channel = await channelManager.fetch(privateVocalChannel.id);
          if (channel) {
            await channel.delete();
          }
        }
      }

      if (await DiscordUserService.removeDiscordUser(member.id)) {
        await LogService.info(`Membre ${member.displayName} ${member.id} supprime en BDD suite a son depart`);
      }

      const departureChannel =
        Config.departureChannelId &&
        ((member.guild.channels.cache.get(Config.departureChannelId) as TextChannel) ||
          ((await member.guild.channels.fetch(Config.departureChannelId)) as TextChannel));

      if (!departureChannel) {
        await LogService.toDeveloper(
          member.client,
          `Salon pour annoncer les départs introuvable suite au départ de ${member.displayName}`
        );
        return await LogService.info(
          `Salon pour annoncer les départs introuvable suite au départ de ${member.displayName}`
        );
      }

      const departureMessage = new EmbedBuilder()
        .setColor(Config.color)
        .setThumbnail(member.user.displayAvatarURL())
        .setTitle(`:outbox_tray: **${member.user.username} a quitté notre communauté**`)
        .setDescription(`Désormais, nous sommes ${member.guild.memberCount} membres`);

      await departureChannel.send({ embeds: [departureMessage] });
    } catch (error) {
      await LogService.handlerError(error as Error, member.client);
    } finally {
      Bot.setActivity(member.client, member.guild);
    }
  }
}
