import { Message, PermissionFlagsBits, TextChannel } from 'discord.js';
import { LogService } from '../services/log.service';
import { RuleService } from '../services/rule.service';

export class MessageCreateEvent {
  public static async run(message: Message): Promise<void> {
    try {
      if (message.author.bot) {
        return;
      }

      if (message.channel.isDMBased()) {
        return;
      }

      if (message.attachments.size > 0 && message.member.permissions.has(PermissionFlagsBits.Administrator)) {
        if (
          message.attachments.some((att) => att.name.toLowerCase() === 'reglement.txt') ||
          message.attachments.some((att) => att.name.toLowerCase() === 'règlement.txt')
        ) {
          await LogService.info('Upload du reglement demarre');
          await message.delete();
          await RuleService.processRulesFile(message.channel as TextChannel, message.attachments);
        }
      }
    } catch (error) {
      await LogService.handlerError(error as Error, message.client);
    }
  }
}
