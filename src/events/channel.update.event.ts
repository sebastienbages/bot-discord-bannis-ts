import { DMChannel, GuildChannel, VoiceChannel } from 'discord.js';
import { PrivateVoiceChannelService } from '../services/private.voice.channel.service';
import { LogService } from '../services/log.service';

export class ChannelUpdateEvent {
  public static async run(oldChannel: DMChannel | GuildChannel, newChannel: DMChannel | GuildChannel): Promise<void> {
    try {
      if (!oldChannel.isVoiceBased()) {
        return;
      }

      const newGuildChannel = newChannel as VoiceChannel;
      const oldGuildChannel = oldChannel as VoiceChannel;

      if (oldGuildChannel.name === newGuildChannel.name) {
        return;
      }

      if (await PrivateVoiceChannelService.updatePrivateVoiceChannel(newGuildChannel)) {
        await LogService.info(
          `Private voice channel ${newGuildChannel.name} ${newGuildChannel.id} mis a jour en BDD pour la guild ${newGuildChannel.guildId}`
        );
      }
    } catch (error) {
      await LogService.handlerError(error as Error, oldChannel.client);
    }
  }
}
