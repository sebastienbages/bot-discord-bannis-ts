import {
  ActionRowBuilder,
  AttachmentBuilder,
  ButtonBuilder,
  ButtonStyle,
  channelLink,
  GuildMember,
  messageLink,
  PermissionFlagsBits,
  TextChannel,
  userMention,
} from 'discord.js';
import { Config } from '../config/config';
import { LogService } from '../services/log.service';
import * as Canvas from 'canvas';
import { MessageActionRowComponentBuilder } from '@discordjs/builders';
import { Bot } from '../bot';
import path from 'node:path';

export class GuildMemberAddEvent {
  public static async run(member: GuildMember): Promise<void> {
    try {
      await LogService.info(`Arrivee d'un nouveau membre : ${member.displayName}`);

      const roleStart =
        member.guild.roles.cache.get(Config.roleFrontiereId) ||
        (await member.guild.roles.fetch(Config.roleFrontiereId));

      if (!roleStart) {
        const admins = member.guild.members.cache.filter(
          (guildMember) => guildMember.permissions.has(PermissionFlagsBits.Administrator) && !guildMember.user.bot
        );

        if (admins.size > 0) {
          await Promise.all(
            admins.map((admin) =>
              admin.send({
                content: `Je n'ai pas reussi à attribuer le role d'arrivee à '${member.user.username}'`,
              })
            )
          );
        }
        await LogService.info(`Echec de l'attribution du role d'arrive a ${member.displayName}`);
        await member.send({
          content:
            "Je n'ai pas réussi à te donner le role pour accéder au discord :confounded:" +
            "\nLes admins sont prévenus, cela devrait s'arranger sous peu :thumbsup:" +
            '\nMerci pour ta patience.',
        });
      } else {
        await member.roles.add(roleStart);
        await LogService.info(`Attribution du role ${roleStart.name} effectue`);
      }

      const welcomeChannel =
        Config.welcomeChannel &&
        ((member.guild.channels.cache.get(Config.welcomeChannel) as TextChannel) ||
          ((await member.guild.channels.fetch(Config.welcomeChannel)) as TextChannel));

      if (!welcomeChannel) {
        await LogService.toDeveloper(
          member.client,
          `Salon de bienvenue introuvable lors de l'arrivé du joueur ${member.displayName}`
        );
        return await LogService.info('Salon textuel de bienvenue introuvable');
      } else {
        Canvas.registerFont(path.join(Config.fontsDir, 'hyborian2.ttf'), { family: 'Hyborian' });

        const welcomeBanner = Canvas.createCanvas(1500, 800);
        const context = welcomeBanner.getContext('2d');

        const background = await Canvas.loadImage(path.join(Config.imageDir, 'banniere_new_player.png'));
        context.drawImage(background, 0, 0, welcomeBanner.width, welcomeBanner.height);
        context.font = '65px Hyborian';
        context.fillStyle = '#e9ba82';
        context.strokeStyle = 'black';
        const textDimensions = context.measureText(member.displayName);
        context.fillText(member.displayName, 750 - textDimensions.width / 2, 560);
        context.strokeText(member.displayName, 750 - textDimensions.width / 2, 560);
        const subtitle = 'a rejoint notre communauté';
        const subtitleDimensions = context.measureText(subtitle);
        context.fillText(subtitle, 750 - subtitleDimensions.width / 2, 620);
        context.strokeText(subtitle, 750 - subtitleDimensions.width / 2, 620);

        context.beginPath();
        context.arc(750, 345, 150, 0, Math.PI * 2, true);
        context.closePath();
        context.clip();

        const avatar = await Canvas.loadImage(member.user.displayAvatarURL({ extension: 'jpg' }));
        context.drawImage(avatar, 600, 195, 300, 300);
        const attachment = new AttachmentBuilder(welcomeBanner.toBuffer(), { name: 'welcome-profile-image.png' });

        const actionRow = new ActionRowBuilder<MessageActionRowComponentBuilder>();

        const rulesChannel =
          Config.rulesChannelId &&
          ((member.guild.channels.cache.get(Config.rulesChannelId) as TextChannel) ||
            ((await member.guild.channels.fetch(Config.rulesChannelId)) as TextChannel));

        if (rulesChannel) {
          actionRow.addComponents(
            new ButtonBuilder()
              .setStyle(ButtonStyle.Link)
              .setLabel('Règlement à valider')
              .setEmoji('📜')
              .setURL(messageLink(Config.rulesChannelId, rulesChannel.lastMessageId, Bot.guildId))
          );
        }

        const borderChannel =
          Config.borderChannelId &&
          ((member.guild.channels.cache.get(Config.borderChannelId) as TextChannel) ||
            ((await member.guild.channels.fetch(Config.borderChannelId)) as TextChannel));

        if (borderChannel) {
          actionRow.addComponents(
            new ButtonBuilder()
              .setStyle(ButtonStyle.Link)
              .setLabel('Si tu as des questions')
              .setEmoji('❔')
              .setURL(channelLink(Config.borderChannelId, Bot.guildId))
          );
        }

        await welcomeChannel.send({
          files: [attachment],
          ...((borderChannel || rulesChannel) && { content: userMention(member.id), components: [actionRow] }),
        });
      }
    } catch (error) {
      await LogService.handlerError(error as Error, member.client);
    } finally {
      Bot.setActivity(member.client, member.guild);
    }
  }
}
