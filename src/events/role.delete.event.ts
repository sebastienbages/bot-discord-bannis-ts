import { Role } from 'discord.js';
import { LogService } from '../services/log.service';
import { DiscordRoleService } from '../services/discord.role.service';
import { PrivateVoiceChannelFeatureService } from '../services/private.voice.channel.feature.service';
import { PrivateVoiceChannelService } from '../services/private.voice.channel.service';

export class RoleDeleteEvent {
  public static async run(role: Role): Promise<void> {
    try {
      const guildId = role.guild.id;

      if (await PrivateVoiceChannelFeatureService.isPremiumRole(role)) {
        await PrivateVoiceChannelFeatureService.removePremiumRole(guildId);
        await PrivateVoiceChannelService.removePremiumFlagToPrivateVoiceChannels(guildId);
        await LogService.info(`Premium role ${role.name} ${role.id} supprime pour la guild ${guildId}`);
      }

      if (await PrivateVoiceChannelFeatureService.hasVipRole(role)) {
        await PrivateVoiceChannelFeatureService.removeVipRole(role);
        await LogService.info(`Vip role ${role.name} ${role.id} supprime pour la guild ${guildId}`);
      }

      if (await DiscordRoleService.removeDiscordRole(role.id)) {
        await LogService.info(`Discord role ${role.name} ${role.id} supprime en BDD`);
      }
    } catch (error) {
      await LogService.handlerError(error as Error, role.client);
    }
  }
}
