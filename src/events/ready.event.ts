import { Client, Guild } from 'discord.js';
import { LogService } from '../services/log.service';
import { Bot } from '../bot';
import { SlashCommandService } from '../services/slash.command.service';
import { VoteService } from '../services/vote.service';
import { PrivateVoiceChannelFeatureService } from '../services/private.voice.channel.feature.service';
import { Readable } from 'node:stream';

export class ReadyEvent {
  public static async run(client: Client): Promise<void> {
    try {
      await SlashCommandService.registerSlashCommands();

      const bannisGuild: Guild = client.guilds.cache.get(Bot.guildId) || (await client.guilds.fetch(Bot.guildId));
      Bot.setActivity(client, bannisGuild);

      await VoteService.initialize(client);

      const guilds = Readable.from(client.guilds.cache.values());

      for await (const guild of guilds as unknown as Guild[]) {
        await PrivateVoiceChannelFeatureService.initialize(guild);
      }
    } catch (error) {
      await client.destroy();
      await LogService.handlerError(error as Error, client);
    }
  }
}
