import { DMChannel, GuildChannel, VoiceChannel } from 'discord.js';
import { LogService } from '../services/log.service';
import { PrivateVoiceChannelService } from '../services/private.voice.channel.service';

export class ChannelDeleteEvent {
  public static async run(channel: DMChannel | GuildChannel): Promise<void> {
    try {
      if (!channel.isVoiceBased()) {
        return;
      }

      const voiceChannel = channel as VoiceChannel;

      if (await PrivateVoiceChannelService.destroyPrivateVoiceChannel(voiceChannel)) {
        await LogService.info(
          `Private voice channel ${voiceChannel.name} ${voiceChannel.id} supprime en BDD pour la guild ${channel.guildId}`
        );
      }
    } catch (error) {
      await LogService.handlerError(error as Error, channel.client);
    }
  }
}
