import { Guild } from 'discord.js';
import { LogService } from '../services/log.service';
import { PrivateVoiceChannelFeatureService } from '../services/private.voice.channel.feature.service';

export class GuildCreateEvent {
  public static async run(guild: Guild): Promise<void> {
    try {
      await LogService.info(`Nouvelle guild ajoutee : ${guild.id}`);
      await PrivateVoiceChannelFeatureService.initialize(guild);
    } catch (error) {
      await LogService.handlerError(error as Error, guild.client);
    }
  }
}
