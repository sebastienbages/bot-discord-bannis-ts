import { Client, Partials, GatewayIntentBits, Guild, ActivityType, Snowflake } from 'discord.js';
import { ReadyEvent } from './events/ready.event';
import { LogService } from './services/log.service';
import { Config } from './config/config';
import { SlashCommandService } from './services/slash.command.service';
import { ButtonService } from './services/button.service';
import { ModalService } from './services/modal.service';
import { SequelizeClient } from './database/client/sequelize.client';
import { pendingMigrations } from './database/migrations/cli/umzug';

export class Bot extends Client {
  public static guildId: Snowflake;
  public static applicationId: Snowflake;

  public constructor(token: string) {
    super({
      intents: [
        GatewayIntentBits.Guilds,
        GatewayIntentBits.GuildMembers,
        GatewayIntentBits.GuildWebhooks,
        GatewayIntentBits.GuildMessages,
        GatewayIntentBits.GuildMessageReactions,
        GatewayIntentBits.GuildIntegrations,
        GatewayIntentBits.GuildPresences,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.DirectMessageTyping,
        GatewayIntentBits.DirectMessageReactions,
        GatewayIntentBits.GuildBans,
        GatewayIntentBits.GuildInvites,
        GatewayIntentBits.GuildEmojisAndStickers,
        GatewayIntentBits.GuildVoiceStates,
        GatewayIntentBits.MessageContent,
      ],
      partials: [Partials.Message, Partials.Channel, Partials.Reaction],
    });
    this.token = token;
  }

  /**
   * Attribue une activité au bot
   *
   * @param client
   * @param guild
   */
  public static setActivity(client: Client, guild: Guild): void {
    client.user.setActivity(`${guild.memberCount ? guild.memberCount.toString() : 'tous les'} membre(s)`, {
      type: ActivityType.Watching,
    });
  }

  /**
   * Démarre le bot
   */
  public async start(): Promise<void> {
    try {
      this.once('ready', async (client: Client) => {
        await LogService.info('Le Bot est en ligne');
        Bot.applicationId = this.application.id;
        Bot.guildId = this.guilds.cache.first().id;
        await ReadyEvent.run(client);
      });

      await Config.checkConfig();
      await SlashCommandService.initialize();
      await ButtonService.initialize();
      await ModalService.initialize();
      await SequelizeClient.initialize();
      await SequelizeClient.testConnection();

      if (await pendingMigrations()) {
        throw new Error('Migration BDD en attente');
      }

      await this.login(this.token);
    } catch (error) {
      if (this.isReady()) {
        await this.destroy();
      }
      await LogService.error(error as Error);
    }
  }
}
