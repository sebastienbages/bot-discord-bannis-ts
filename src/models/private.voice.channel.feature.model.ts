import {
  Association,
  BelongsToManyAddAssociationMixin,
  BelongsToManyCountAssociationsMixin,
  BelongsToManyGetAssociationsMixin,
  BelongsToManyHasAssociationMixin,
  BelongsToManyRemoveAssociationMixin,
  CreationOptional,
  DataTypes,
  HasManyCountAssociationsMixin,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from 'sequelize';
import { Snowflake } from 'discord.js';
import { SequelizeClient } from '../database/client/sequelize.client';
import { DiscordRoleModel } from './discord.role.model';
import { PrivateVoiceChannelModel } from './private.voice.channel.model';

export class PrivateVoiceChannelFeatureModel extends Model<
  InferAttributes<PrivateVoiceChannelFeatureModel>,
  InferCreationAttributes<PrivateVoiceChannelFeatureModel>
> {
  public declare static associations: {
    vipRoles: Association<PrivateVoiceChannelFeatureModel, DiscordRoleModel>;
    privateVoiceChannels: Association<PrivateVoiceChannelFeatureModel, PrivateVoiceChannelModel>;
  };

  public declare vipRoles?: NonAttribute<DiscordRoleModel[]>;
  public declare addVipRole: BelongsToManyAddAssociationMixin<DiscordRoleModel, Snowflake>;
  public declare removeVipRole: BelongsToManyRemoveAssociationMixin<DiscordRoleModel, Snowflake>;
  public declare getVipRoles: BelongsToManyGetAssociationsMixin<DiscordRoleModel>;
  public declare hasVipRole: BelongsToManyHasAssociationMixin<DiscordRoleModel, Snowflake>;
  public declare countVipRoles: BelongsToManyCountAssociationsMixin;

  public declare privateVoiceChannels?: NonAttribute<PrivateVoiceChannelModel[]>;
  public declare countPrivateVoiceChannels: HasManyCountAssociationsMixin;

  public declare guildId: Snowflake;
  public declare timeLimitInHours: number;
  public declare isActive: boolean;
  public declare categoryId?: CreationOptional<Snowflake>;
  public declare maxVoiceChannelInGuild: number;
  public declare maxVoiceChannelByUser: number;
  public declare premiumDiscordRoleId?: CreationOptional<Snowflake>;

  public static initialize() {
    PrivateVoiceChannelFeatureModel.init(
      {
        guildId: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        timeLimitInHours: {
          type: DataTypes.INTEGER,
          defaultValue: 12,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        categoryId: {
          type: new DataTypes.STRING(),
          allowNull: true,
        },
        maxVoiceChannelInGuild: {
          type: DataTypes.INTEGER,
          defaultValue: 50,
        },
        maxVoiceChannelByUser: {
          type: DataTypes.INTEGER,
          defaultValue: 1,
        },
        premiumDiscordRoleId: {
          type: new DataTypes.STRING(),
          allowNull: true,
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'privateVoiceChannelFeature' }
    );
  }
}
