import axios, { AxiosError } from 'axios';
import { Config } from '../config/config';
import { InteractionError } from '../error/interaction.error';
import { bold } from 'discord.js';

export interface TopServerInfos {
  code: number;
  success: boolean;
  server: Server;
}

export interface TopServerPlayerRanking {
  code: number;
  success: boolean;
  players: TopServerMember[];
}

export interface TopServerStats {
  code: number;
  success: boolean;
  stats: {
    monthly: MonthlyStats[];
    weekly: WeeklyStats[];
  };
}

export interface TopServerError {
  code: number;
  success: boolean;
  message: string;
}

declare interface Server {
  top_id: number;
  name: string;
  slug: string;
  website: string;
  short_description: string;
  ip: string;
  port: number;
  slots: number;
  access: string;
  teamspeak_ip: string;
  teamspeak_port: number;
  mumble_ip: string;
  mumble_port: number;
  discord: string;
  total_clics: number;
  total_votes: number;
  total_likes: number;
  facebook: string;
  twitter: string;
  google: string;
  youtube: string;
  created_at: string;
  updated_at: string;
  players: string;
  online: number;
  plugins: string;
  twitch_login: string;
  twitch_live: number;
  display_lives: number;
  discord_webhook: string;
  show_ip: number;
  last_check: string;
  map: string;
  version: string;
  nb_fails: number;
  query_port: number;
  trailer: string;
  down_at: string;
  record_players: number;
}

export interface TopServerMember {
  votes: number;
  playername: string;
}

interface MonthlyStats {
  year: number;
  january_votes: number;
  february_votes: number;
  march_votes: number;
  april_votes: number;
  may_votes: number;
  june_votes: number;
  july_votes: number;
  august_votes: number;
  september_votes: number;
  october_votes: number;
  november_votes: number;
  december_votes: number;
  january_clics: number;
  february_clics: number;
  march_clics: number;
  april_clics: number;
  may_clics: number;
  june_clics: number;
  july_clics: number;
  august_clics: number;
  september_clics: number;
  october_clics: number;
  november_clics: number;
  december_clics: number;
}

interface WeeklyStats {
  year: number;
  // eslint-disable-next-line id-blacklist
  number: number;
  monday_votes: number;
  tuesday_votes: number;
  wednesday_votes: number;
  thursday_votes: number;
  friday_votes: number;
  saturday_votes: number;
  sunday_votes: number;
  monday_clics: number;
  tuesday_clics: number;
  wednesday_clics: number;
  thursday_clics: number;
  friday_clics: number;
  saturday_clics: number;
  sunday_clics: number;
}

export class TopServerModel {
  private static readonly baseUrl = `https://api.top-serveurs.net/v1/servers/${Config.tokenTopServer}`;
  private static readonly playerRankingUrl = `${this.baseUrl}/players-ranking`;
  private static readonly serverStatsUrl = `${this.baseUrl}/stats`;

  /**
   * Récupère les informations Top Serveur
   */
  public static async getDataServer(): Promise<TopServerInfos> {
    try {
      const { data } = await axios.get<TopServerInfos>(this.baseUrl);
      return data;
    } catch (error) {
      const axiosError = error as AxiosError<TopServerError>;
      throw new InteractionError(
        `L'API de Top Serveur m'a retournée une erreur :weary:\nVoici son message : ${bold(
          axiosError.response.data.message
        )}`,
        'getDataServer',
        axiosError.response.data.message
      );
    }
  }

  /**
   * Récupère la liste des votants du mois courant et leur nombre de votes
   */
  public static async getPlayersRankingForCurrentMonth(): Promise<TopServerPlayerRanking> {
    try {
      const { data } = await axios.get<TopServerPlayerRanking>(this.playerRankingUrl);
      return data;
    } catch (error) {
      const axiosError = error as AxiosError<TopServerError>;
      throw new InteractionError(
        `L'API de Top Serveur m'a retournée une erreur :weary:\nVoici son message : ${bold(
          axiosError.response.data.message
        )}`,
        'getPlayersRankingForCurrentMonth',
        axiosError.response.data.message
      );
    }
  }

  /**
   * Récupère la liste des votants du mois dernier et leur nombre de votes
   */
  public static async getPlayersRankingForLastMonth(): Promise<TopServerPlayerRanking> {
    try {
      const { data } = await axios.get<TopServerPlayerRanking>(`${this.playerRankingUrl}?type=lastMonth`);
      return data;
    } catch (error) {
      const axiosError = error as AxiosError<TopServerError>;
      throw new InteractionError(
        `L'API de Top Serveur m'a retournée une erreur :weary:\nVoici son message : ${bold(
          axiosError.response.data.message
        )}`,
        'getPlayersRankingForLastMonth',
        axiosError.response.data.message
      );
    }
  }

  /**
   * Récupère les statistiques du serveur
   */
  public static async getServerStats(): Promise<TopServerStats> {
    try {
      const { data } = await axios.get<TopServerStats>(this.serverStatsUrl);
      return data;
    } catch (error) {
      const axiosError = error as AxiosError<TopServerError>;
      throw new InteractionError(
        `L'API de Top Serveur m'a retournée une erreur :weary:\nVoici son message : ${bold(
          axiosError.response.data.message
        )}`,
        'getServerStats',
        axiosError.response.data.message
      );
    }
  }
}
