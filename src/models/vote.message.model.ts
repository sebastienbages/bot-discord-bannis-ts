import { Snowflake } from 'discord.js';
import { DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import { SequelizeClient } from '../database/client/sequelize.client';

export class VoteMessageModel extends Model<
  InferAttributes<VoteMessageModel>,
  InferCreationAttributes<VoteMessageModel>
> {
  public declare id: Snowflake;
  public declare channel: Snowflake;
  public declare guild: Snowflake;

  public static initialize() {
    VoteMessageModel.init(
      {
        id: {
          type: new DataTypes.STRING(18),
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
        },
        channel: {
          type: new DataTypes.STRING(18),
          allowNull: false,
          autoIncrement: false,
        },
        guild: {
          type: new DataTypes.STRING(18),
          allowNull: false,
          autoIncrement: false,
          primaryKey: true,
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'voteMessage' }
    );
  }
}
