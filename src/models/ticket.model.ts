import { Snowflake } from 'discord.js';
import { CreationOptional, DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import { SequelizeClient } from '../database/client/sequelize.client';

export class TicketModel extends Model<InferAttributes<TicketModel>, InferCreationAttributes<TicketModel>> {
  public declare count: number;
  public declare userId: Snowflake;
  public declare guildId: Snowflake;
  public declare subject: string;
  public declare server: string;
  public declare when: string;
  public declare isOpen: CreationOptional<boolean>;
  public declare isDeleted: CreationOptional<boolean>;
  public declare createdAt: CreationOptional<Date>;
  public declare updatedAt: CreationOptional<Date>;

  public static initialize() {
    TicketModel.init(
      {
        count: {
          type: DataTypes.INTEGER,
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        userId: {
          type: new DataTypes.STRING(18),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        guildId: {
          type: new DataTypes.STRING(18),
          allowNull: false,
        },
        subject: {
          type: new DataTypes.STRING(4000),
          allowNull: false,
        },
        server: {
          type: new DataTypes.STRING(4000),
          allowNull: false,
        },
        when: {
          type: new DataTypes.STRING(4000),
          allowNull: false,
        },
        isOpen: {
          type: DataTypes.BOOLEAN,
          defaultValue: true,
        },
        isDeleted: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
      },
      { sequelize: SequelizeClient.client, tableName: 'ticket' }
    );
  }
}
