import { CreationOptional, DataTypes, InferAttributes, InferCreationAttributes, Model } from 'sequelize';
import { Snowflake } from 'discord.js';
import { SequelizeClient } from '../database/client/sequelize.client';

export class VoteConfigModel extends Model<InferAttributes<VoteConfigModel>, InferCreationAttributes<VoteConfigModel>> {
  public declare guildId: Snowflake;
  public declare channelId: Snowflake;
  public declare timer: number;
  public declare isActive: CreationOptional<boolean>;
  public declare updatedAt: Date;

  public static initialize() {
    VoteConfigModel.init(
      {
        guildId: {
          type: new DataTypes.STRING(18),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        channelId: {
          type: new DataTypes.STRING(18),
          autoIncrement: false,
          allowNull: false,
        },
        timer: {
          type: DataTypes.INTEGER,
          autoIncrement: false,
          allowNull: false,
        },
        isActive: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        updatedAt: {
          type: DataTypes.DATE,
          allowNull: false,
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'voteConfig' }
    );
  }
}
