import {
  Association,
  BelongsToManyAddAssociationMixin,
  BelongsToManyCountAssociationsMixin,
  BelongsToManyGetAssociationsMixin,
  BelongsToManyHasAssociationMixin,
  BelongsToManyRemoveAssociationMixin,
  CreationOptional,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from 'sequelize';
import { Snowflake } from 'discord.js';
import { SequelizeClient } from '../database/client/sequelize.client';
import { DiscordUserModel } from './discord.user.model';
import { PrivateVoiceChannelFeatureModel } from './private.voice.channel.feature.model';

export class PrivateVoiceChannelModel extends Model<
  InferAttributes<PrivateVoiceChannelModel>,
  InferCreationAttributes<PrivateVoiceChannelModel>
> {
  public declare static associations: {
    discordUser: Association<PrivateVoiceChannelModel, DiscordUserModel>;
    privateVoiceChannelFeature: Association<PrivateVoiceChannelModel, PrivateVoiceChannelFeatureModel>;
    discordUserGuests: Association<PrivateVoiceChannelModel, DiscordUserModel>;
  };

  public declare discordUser?: NonAttribute<DiscordUserModel>;

  public declare privateVoiceChannelFeature?: NonAttribute<PrivateVoiceChannelFeatureModel>;

  public declare discordUserGuests?: NonAttribute<DiscordUserModel[]>;
  public declare addDiscordUserGuest: BelongsToManyAddAssociationMixin<DiscordUserModel, Snowflake>;
  public declare removeDiscordUserGuest: BelongsToManyRemoveAssociationMixin<DiscordUserModel, Snowflake>;
  public declare hasDiscordUserGuest: BelongsToManyHasAssociationMixin<DiscordUserModel, Snowflake>;
  public declare getDiscordUserGuests: BelongsToManyGetAssociationsMixin<DiscordUserModel>;
  public declare countDiscordUserGuests: BelongsToManyCountAssociationsMixin;

  public declare id: Snowflake;
  public declare name: string;
  public declare isPremium: CreationOptional<boolean>;
  public declare createdAt: CreationOptional<Date>;
  public declare discordUserId: Snowflake;
  public declare privateVoiceChannelFeatureGuildId: Snowflake;

  public static initialize() {
    PrivateVoiceChannelModel.init(
      {
        id: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        name: {
          type: new DataTypes.STRING(100),
          autoIncrement: false,
          allowNull: false,
        },
        isPremium: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        createdAt: {
          type: DataTypes.DATE,
          allowNull: false,
          defaultValue: new Date(),
        },
        discordUserId: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          references: {
            model: 'discordUser',
            key: 'id',
          },
        },
        privateVoiceChannelFeatureGuildId: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          references: {
            model: 'privateVoiceChannelFeature',
            key: 'guildId',
          },
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'privateVoiceChannel' }
    );
  }
}
