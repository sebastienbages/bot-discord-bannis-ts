import {
  Association,
  BelongsToManyCountAssociationsMixin,
  DataTypes,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from 'sequelize';
import { Snowflake } from 'discord.js';
import { SequelizeClient } from '../database/client/sequelize.client';
import { PrivateVoiceChannelFeatureModel } from './private.voice.channel.feature.model';

export class DiscordRoleModel extends Model<
  InferAttributes<DiscordRoleModel>,
  InferCreationAttributes<DiscordRoleModel>
> {
  public declare static associations: {
    vipPrivateVoiceChannelFeatures: Association<PrivateVoiceChannelFeatureModel, DiscordRoleModel>;
  };

  public declare vipPrivateVoiceChannelFeatures?: NonAttribute<PrivateVoiceChannelFeatureModel[]>;
  public declare countVipPrivateVoiceChannelFeatures: BelongsToManyCountAssociationsMixin;

  public declare id: Snowflake;

  public static initialize() {
    DiscordRoleModel.init(
      {
        id: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'discordRole' }
    );
  }

  public async hasAssociations(): Promise<boolean> {
    const vipPrivateVoiceChannelFeatureCount = await this.countVipPrivateVoiceChannelFeatures();
    return vipPrivateVoiceChannelFeatureCount > 0;
  }
}
