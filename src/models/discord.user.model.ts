import {
  Association,
  DataTypes,
  HasManyCountAssociationsMixin,
  InferAttributes,
  InferCreationAttributes,
  Model,
  NonAttribute,
} from 'sequelize';
import { Snowflake } from 'discord.js';
import { SequelizeClient } from '../database/client/sequelize.client';
import { PrivateVoiceChannelModel } from './private.voice.channel.model';

export class DiscordUserModel extends Model<
  InferAttributes<DiscordUserModel>,
  InferCreationAttributes<DiscordUserModel>
> {
  public declare static associations: {
    privateVoiceChannels: Association<PrivateVoiceChannelModel, DiscordUserModel>;
    privateVoiceChannelsGuest: Association<PrivateVoiceChannelModel, DiscordUserModel>;
  };

  public declare privateVoiceChannels?: NonAttribute<PrivateVoiceChannelModel[]>;
  public declare countPrivateVoiceChannels: HasManyCountAssociationsMixin;

  public declare privateVoiceChannelsGuest?: NonAttribute<PrivateVoiceChannelModel[]>;
  public declare countPrivateVoiceChannelsGuest: HasManyCountAssociationsMixin;

  public declare id: Snowflake;
  public declare name: string;

  public static initialize() {
    DiscordUserModel.init(
      {
        id: {
          type: new DataTypes.STRING(),
          autoIncrement: false,
          allowNull: false,
          primaryKey: true,
        },
        name: {
          type: new DataTypes.STRING(32),
          autoIncrement: false,
          allowNull: false,
        },
      },
      { sequelize: SequelizeClient.client, tableName: 'discordUser' }
    );
  }

  public async hasAssociations(): Promise<boolean> {
    const privateVoiceChannelsCount = await this.countPrivateVoiceChannels();
    const privateVoiceChannelsGuestCount = await this.countPrivateVoiceChannelsGuest();
    return privateVoiceChannelsCount > 0 || privateVoiceChannelsGuestCount > 0;
  }
}
