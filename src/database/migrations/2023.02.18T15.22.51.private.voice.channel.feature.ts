import { MigrationFn } from 'umzug';
import { DataTypes, Sequelize } from 'sequelize';

export const up: MigrationFn<Sequelize> = async ({ context: sequelize }) => {
  const queryInterface = sequelize.getQueryInterface();
  // -------------------------------------------------------------------
  // TABLES
  // -------------------------------------------------------------------
  await queryInterface.createTable('privateVoiceChannelFeature', {
    guildId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
    },
    timeLimitInHours: {
      type: DataTypes.INTEGER,
      defaultValue: 12,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    categoryId: {
      type: new DataTypes.STRING(18),
      allowNull: true,
    },
    maxVoiceChannelInGuild: {
      type: DataTypes.INTEGER,
      defaultValue: 50,
    },
    maxVoiceChannelByUser: {
      type: DataTypes.INTEGER,
      defaultValue: 1,
    },
    premiumDiscordRoleId: {
      type: new DataTypes.STRING(),
      allowNull: true,
    },
  });
  await queryInterface.createTable('discordRole', {
    id: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
    },
  });
  await queryInterface.createTable('discordUser', {
    id: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
    },
    name: {
      type: new DataTypes.STRING(32),
      autoIncrement: false,
      allowNull: false,
      onDelete: 'CASCADE',
    },
  });
  await queryInterface.createTable('privateVoiceChannel', {
    id: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
    },
    name: {
      type: new DataTypes.STRING(100),
      autoIncrement: false,
      allowNull: false,
      onDelete: 'CASCADE',
    },
    isPremium: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: DataTypes.DATE,
  });
  // -------------------------------------------------------------------
  // ASSOCIATIONS
  // -------------------------------------------------------------------
  // Many-To-Many DISCORD ROLE - VOICE CHANNEL GUILD FEATURE (VIP ROLES)
  await queryInterface.createTable('privateVoiceChannelFeatureVipRole', {
    discordRoleId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
      references: {
        model: 'discordRole',
        key: 'id',
      },
    },
    privateVoiceChannelFeatureGuildId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
      references: {
        model: 'privateVoiceChannelFeature',
        key: 'guildId',
      },
    },
  });
  // One-To-Many DISCORD USER - DISCORD VOICE CHANNEL (VOICE CHANNEL OWNER)
  await queryInterface.addColumn('privateVoiceChannel', 'discordUserId', {
    type: new DataTypes.STRING(18),
    autoIncrement: false,
    allowNull: false,
    onDelete: 'CASCADE',
    references: {
      model: 'discordUser',
      key: 'id',
    },
  });
  // One-To-Many VOICE CHANNEL GUILD FEATURE - DISCORD VOICE CHANNEL (PRIVATE VOICE CHANNEL FEATURE OWNER)
  await queryInterface.addColumn('privateVoiceChannel', 'privateVoiceChannelFeatureGuildId', {
    type: new DataTypes.STRING(18),
    autoIncrement: false,
    allowNull: false,
    onDelete: 'CASCADE',
    references: {
      model: 'privateVoiceChannelFeature',
      key: 'guildId',
    },
  });
  // Many-To-Many DISCORD USER - DISCORD VOICE CHANNEL (GUESTS LIST)
  await queryInterface.createTable('privateVoiceChannelGuest', {
    discordUserId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
      references: {
        model: 'discordUser',
        key: 'id',
      },
    },
    privateVoiceChannelId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
      onDelete: 'CASCADE',
      references: {
        model: 'privateVoiceChannel',
        key: 'id',
      },
    },
  });
};
export const down: MigrationFn<Sequelize> = async ({ context: sequelize }) => {
  const queryInterface = sequelize.getQueryInterface();
  await queryInterface.dropTable('privateVoiceChannelFeatureVipRole');
  await queryInterface.dropTable('privateVoiceChannelGuest');
  await queryInterface.dropTable('privateVoiceChannel');
  await queryInterface.dropTable('discordUser');
  await queryInterface.dropTable('discordRole');
  await queryInterface.dropTable('privateVoiceChannelFeature');
};
