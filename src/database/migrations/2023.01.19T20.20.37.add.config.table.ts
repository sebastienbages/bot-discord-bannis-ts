import { MigrationFn } from 'umzug';
import { DataTypes, Sequelize } from 'sequelize';

export const up: MigrationFn = async ({ context: sequelize }) => {
  const sqlize = sequelize as Sequelize;
  await sqlize.getQueryInterface().createTable('voteConfig', {
    guildId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
    },
    channelId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
    },
    timer: {
      type: DataTypes.INTEGER,
      autoIncrement: false,
      allowNull: false,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  });
};
export const down: MigrationFn = async ({ context: sequelize }) => {
  const sqlize = sequelize as Sequelize;
  await sqlize.getQueryInterface().dropTable('voteConfig');
};
