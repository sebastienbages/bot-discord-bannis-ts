import { Umzug, SequelizeStorage } from 'umzug';
import { Sequelize } from 'sequelize';
import { SequelizeClient } from '../../client/sequelize.client';
import { Config } from '../../../config/config';
import * as fs from 'node:fs';

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: SequelizeClient.dbName,
  define: {
    timestamps: false,
    freezeTableName: true,
  },
});

export const migrator = new Umzug({
  migrations: {
    glob:
      Config.nodeEnv === Config.nodeEnvValues.production
        ? 'src/database/migrations/*.js'
        : 'dist/src/database/migrations/*.js',
  },
  create: {
    template: (filepath) => [
      [filepath, fs.readFileSync('./src/database/migrations/model/migration.model.ts').toString()],
    ],
  },
  context: sequelize,
  storage: new SequelizeStorage({
    sequelize,
  }),
  logger: undefined,
});

export const pendingMigrations = async (): Promise<boolean> => (await migrator.pending()).length > 0;

export type Migration = typeof migrator._types.migration;
