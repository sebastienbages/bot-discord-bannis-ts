import { MigrationFn } from 'umzug';
import { DataTypes, Sequelize } from 'sequelize';

export const up: MigrationFn = async ({ context: sequelize }) => {
  const sqlize = sequelize as Sequelize;
  await sqlize.getQueryInterface().createTable('ticket', {
    count: {
      type: DataTypes.INTEGER,
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
    },
    userId: {
      type: new DataTypes.STRING(18),
      autoIncrement: false,
      allowNull: false,
      primaryKey: true,
    },
    guildId: {
      type: new DataTypes.STRING(18),
      allowNull: false,
    },
    subject: {
      type: new DataTypes.STRING(4000),
      allowNull: false,
    },
    server: {
      type: new DataTypes.STRING(4000),
      allowNull: false,
    },
    when: {
      type: new DataTypes.STRING(4000),
      allowNull: false,
    },
    isOpen: {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  });
  await sqlize.getQueryInterface().createTable('voteMessage', {
    id: {
      type: new DataTypes.STRING(18),
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
    },
    channel: {
      type: new DataTypes.STRING(18),
      allowNull: false,
      autoIncrement: false,
    },
    guild: {
      type: new DataTypes.STRING(18),
      allowNull: false,
      autoIncrement: false,
      primaryKey: true,
    },
  });
};
export const down: MigrationFn = async ({ context: sequelize }) => {
  const sqlize = sequelize as Sequelize;
  await sqlize.getQueryInterface().dropTable('ticket');
  await sqlize.getQueryInterface().dropTable('voteMessage');
};
