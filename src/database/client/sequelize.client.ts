import { LogService } from '../../services/log.service';
import { Sequelize } from 'sequelize';
import { VoteMessageModel } from '../../models/vote.message.model';
import { TicketModel } from '../../models/ticket.model';
import { Config } from '../../config/config';
import { VoteConfigModel } from '../../models/vote.config.model';
import { PrivateVoiceChannelFeatureModel } from '../../models/private.voice.channel.feature.model';
import { DiscordRoleModel } from '../../models/discord.role.model';
import { DiscordUserModel } from '../../models/discord.user.model';
import { PrivateVoiceChannelModel } from '../../models/private.voice.channel.model';
export class SequelizeClient {
  public static client: Sequelize;
  public static readonly dbName =
    Config.nodeEnv === Config.nodeEnvValues.production
      ? `/etc/bannis-bot-discord-sqlite/${Config.DB.name}.sqlite`
      : `./${Config.DB.name}.sqlite`;

  public static async initialize(): Promise<void> {
    this.client = new Sequelize({
      dialect: 'sqlite',
      storage: this.dbName,
      logging: false,
      define: {
        timestamps: false,
        freezeTableName: true,
      },
    });
    this.initModels();
    this.initPrivateVoiceChannelFeatureAssociations();
    await LogService.info('Database initialized');
  }

  public static async testConnection(): Promise<void> {
    try {
      await this.client.authenticate();
      return LogService.info('Successful database authentication');
    } catch (error) {
      await LogService.error(error as Error);
      throw new Error('Database authentication failed');
    }
  }

  /**
   * Init models
   *
   * @private
   */
  private static initModels() {
    VoteMessageModel.initialize();
    TicketModel.initialize();
    VoteConfigModel.initialize();
    PrivateVoiceChannelFeatureModel.initialize();
    DiscordRoleModel.initialize();
    DiscordUserModel.initialize();
    PrivateVoiceChannelModel.initialize();
  }

  /**
   * Init associations
   *
   * @private
   */
  private static initPrivateVoiceChannelFeatureAssociations() {
    // -------------------------------------------------------------------
    // PRIVATE VOICE CHANNEL FEATURE
    // -------------------------------------------------------------------
    // VIP ROLES
    DiscordRoleModel.belongsToMany(PrivateVoiceChannelFeatureModel, {
      through: 'privateVoiceChannelFeatureVipRole',
      foreignKey: 'discordRoleId',
      as: 'vipPrivateVoiceChannelFeatures',
    });
    PrivateVoiceChannelFeatureModel.belongsToMany(DiscordRoleModel, {
      through: 'privateVoiceChannelFeatureVipRole',
      foreignKey: 'privateVoiceChannelFeatureGuildId',
      as: 'vipRoles',
    });
    // VOICE CHANNEL OWNER
    DiscordUserModel.hasMany(PrivateVoiceChannelModel, {
      sourceKey: 'id',
      foreignKey: 'discordUserId',
      as: 'privateVoiceChannels',
    });
    PrivateVoiceChannelModel.belongsTo(DiscordUserModel, {
      targetKey: 'id',
      foreignKey: 'discordUserId',
      as: 'discordUser',
    });
    // PRIVATE VOICE CHANNEL FEATURE OWNER
    PrivateVoiceChannelFeatureModel.hasMany(PrivateVoiceChannelModel, {
      sourceKey: 'guildId',
      foreignKey: 'privateVoiceChannelFeatureGuildId',
      as: 'privateVoiceChannels',
    });
    PrivateVoiceChannelModel.belongsTo(PrivateVoiceChannelFeatureModel, {
      targetKey: 'guildId',
      foreignKey: 'privateVoiceChannelFeatureGuildId',
      as: 'privateVoiceChannelFeature',
    });
    // PRIVATE VOICE CHANNEL GUESTS
    DiscordUserModel.belongsToMany(PrivateVoiceChannelModel, {
      through: 'privateVoiceChannelGuest',
      foreignKey: 'discordUserId',
      as: 'privateVoiceChannelsGuest',
    });
    PrivateVoiceChannelModel.belongsToMany(DiscordUserModel, {
      through: 'privateVoiceChannelGuest',
      foreignKey: 'privateVoiceChannelId',
      as: 'discordUserGuests',
    });
  }
}
