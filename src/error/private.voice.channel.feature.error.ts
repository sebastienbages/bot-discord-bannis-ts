import { GuildMember, VoiceChannel } from 'discord.js';
import { InteractionError } from './interaction.error';

export const getWrongPrivateVoiceChannelOwnerError = (
  channel: VoiceChannel,
  guildMember: GuildMember
): InteractionError =>
  new InteractionError(
    "Ce salon ne t'appartient pas :unamused:",
    'wrongPrivateVoiceChannel',
    `le salon ${channel.name} ${channel.id} n'appartient pas a ${guildMember.displayName} ${guildMember.id}`
  );
